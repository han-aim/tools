from __future__ import annotations

from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser, BooleanOptionalAction, Namespace
from asyncio import run
from os import environ
from sys import stderr
from tempfile import mkdtemp

from anyio import Path
from dotenv import find_dotenv, load_dotenv
from kubernetes import client
from loguru import logger

from aimtools import __doc__ as aimtools__doc__
from aimtools import check, container, kubernetes, product
from aimtools.__version__ import __version__
from aimtools.check import jupyter
from aimtools.container import login
from aimtools.container.build import BuildDocker, BuildNerdctl, BuildPodman
from aimtools.kubernetes import (
    adapt_wsl,
    create_namespace,
    debug,
    delete_namespace,
    lifecycle,
    load_kubeconfig,
    recreate_basicauth,
    recreate_sshkeys,
    run_module_under_wsl,
    taloslinux,
)
from aimtools.kubernetes.lifecycle import Lifecycle
from aimtools.kubernetes.taloslinux import Taloslinux
from aimtools.kubernetes.taloslinux import cli as taloslinux_cli
from aimtools.product import produce_revision, provision


@logger.catch(reraise=True)
# The complexity is essential but it could be broken down further. Allow for now, since this structure is rather usual.
async def main() -> None:  # noqa: C901, PLR0912, PLR0915
    str_path_file_dotenv = find_dotenv(usecwd=True)
    was_dotenv_loaded = load_dotenv(dotenv_path=str_path_file_dotenv, verbose=True)
    argumentparser = ArgumentParser(
        description=aimtools__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    subcommand = argumentparser.add_subparsers(
        dest="subcommand",
        help="Subcommand.",
    )
    argumentparser_check = subcommand.add_parser(
        name="check",
        description=check.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    argumentparser_check = create_subcommand_check(argumentparser_check=argumentparser_check)
    argumentparser_product = subcommand.add_parser(
        name="product",
        description=product.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    argumentparser_product = create_subcommand_product(argumentparser_product=argumentparser_product)
    argumentparser_container = subcommand.add_parser(
        name="container",
        description=container.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    argumentparser_container = create_subcommand_container(argumentparser_container=argumentparser_container)
    argumentparser_kubernetes = subcommand.add_parser(
        name="kubernetes",
        description=kubernetes.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    argumentparser_kubernetes = create_subcommand_kubernetes(argumentparser_kubernetes=argumentparser_kubernetes)
    argumentparser.add_argument(
        "--path_dir_output",
        type=Path,
        default=None,
        help=(
            "Directory path under which to put generic output, mainly the log file. If `None`, generate a temporary "
            "directory under the default OS-dependent temporary directory."
        ),
    )
    parsedargs = argumentparser.parse_args()
    str_path_dir_output = mkdtemp(dir=parsedargs.path_dir_output, prefix="aimtools--")
    path_dir_output = Path(str_path_dir_output)
    path_file_log = path_dir_output / "output.log"
    logger.configure(
        handlers=[
            {
                "backtrace": True,
                "enqueue": True,
                "diagnose": False,
                "format": "{time} - {level} - {name} - {function} - {process} - {thread} - {message}",
                "serialize": False,
                "sink": path_file_log,
            },
            {
                "backtrace": False,
                "enqueue": True,
                "diagnose": False,
                "format": "<d>{time}</d> - <red>{level}</red> - {name} - {function} - <b>{message}</b>",
                "serialize": False,
                "sink": stderr,
            },
        ],
    )
    logger.info("AIM Tools revision or version: {}.", __version__)
    logger.info("Also logging to file '{!s}'.", await path_file_log.absolute())
    logger.info(
        "`.env` at '{}' was {} loaded.",
        str_path_file_dotenv,
        "" if was_dotenv_loaded else "not",
    )
    match parsedargs:
        case Namespace(subcommand="kubernetes"):
            load_kubeconfig()
            corev1api = client.CoreV1Api()
            match parsedargs:
                case Namespace(
                    subcommand_kubernetes="adapt_wsl",
                    path_userprofile_maybe=path_dir_userprofile,
                    path_input=path_input,
                ):
                    await adapt_wsl.subcommand(path_dir_userprofile=path_dir_userprofile, path_input=path_input)
                case Namespace(subcommand_kubernetes="create_namespace", name=name):
                    create_namespace.subcommand(
                        corev1api=corev1api,
                        namespace=name,
                    )
                case Namespace(subcommand_kubernetes="debug"):
                    await debug.subcommand(path_dir_output=path_dir_output)
                case Namespace(subcommand_kubernetes="delete_namespace", name=name):
                    delete_namespace.subcommand(
                        corev1api=corev1api,
                        namespace=name,
                    )
                case Namespace(
                    subcommand_kubernetes="recreate_basicauth",
                    namespace=namespace,
                    name=name,
                    username=username,
                ):
                    recreate_basicauth.subcommand(
                        corev1api=corev1api,
                        namespace=namespace,
                        name=name,
                        username=username,
                    )
                case Namespace(subcommand_kubernetes="recreate_sshkeys", namespace=namespace):
                    recreate_sshkeys.subcommand(
                        corev1api=corev1api,
                        namespace=namespace,
                    )
                case Namespace(
                    subcommand_kubernetes="run_module_under_wsl",
                    path_dir_working=path_dir_working,
                    modulepath=modulepath,
                ):
                    await run_module_under_wsl.subcommand(
                        path_dir_working=path_dir_working,
                        modulepath=modulepath,
                    )
                case Namespace(
                    path_dir_kustomize=path_dir_kustomize,
                    subcommand_kubernetes="lifecycle",
                    site=site,
                    subcommand_kubernetes_lifecycle=subcommand_kubernetes_lifecycle,
                ):
                    await lifecycle.subcommand(
                        site=site,
                        subcommand_kubernetes_lifecycle=subcommand_kubernetes_lifecycle,
                        path_dir_kustomize=path_dir_kustomize,
                    )
                case Namespace(subcommand_kubernetes="taloslinux"):
                    await taloslinux_cli.subcommand(
                        subcommand_taloslinux=parsedargs.subcommand_kubernetes_taloslinux,
                        path_dir_configuration=parsedargs.path_dir_configuration,
                        generate_taloslinux_configuration=parsedargs.generate_taloslinux_configuration,
                    )
        case Namespace(subcommand="container"):
            # TODO: Reduce use of env vars, since they're hard to control.
            architecture = environ["ARCHITECTURE"]
            name_product = environ["NAME_PRODUCT"]
            os = environ["OS"]
            registry_image_base = environ["REGISTRY_IMAGE_BASE"]
            target = environ["TARGET"]
            name_image = environ.get("NAME_IMAGE", name_product)
            registry_image_product = environ["REGISTRY_IMAGE_PRODUCT"]
            _, _, revision = (await produce_revision()).partition("+git.")
            if not revision:
                message = "Could not parse revision."
                raise ValueError(message)
            match parsedargs:
                case Namespace(subcommand_container="build", tool="docker"):
                    builddocker = BuildDocker(
                        architecture=architecture,
                        name_image=name_image,
                        name_product=name_product,
                        os=os,
                        path_dir_output=path_dir_output,
                        registry_image_base=registry_image_base,
                        registry_image_product=registry_image_product,
                        revision=revision,
                        target=target,
                    )
                    await builddocker.run()
                case Namespace(subcommand_container="build", tool="nerdctl"):
                    buildnerdctl = BuildNerdctl(
                        architecture=architecture,
                        name_image=name_image,
                        name_product=name_product,
                        os=os,
                        path_dir_output=path_dir_output,
                        registry_image_base=registry_image_base,
                        registry_image_product=registry_image_product,
                        revision=revision,
                        target=target,
                    )
                    await buildnerdctl.run()
                case Namespace(subcommand_container="build" | "manifest", tool="podman"):
                    buildpodman = BuildPodman(
                        architecture=architecture,
                        name_image=name_image,
                        name_product=name_product,
                        os=os,
                        path_dir_output=path_dir_output,
                        registry_image_base=registry_image_base,
                        registry_image_product=registry_image_product,
                        revision=revision,
                        target=target,
                    )
                    match parsedargs:
                        case Namespace(subcommand_container="build", tool="podman"):
                            await buildpodman.run()
                        case Namespace(subcommand_container="manifest", tool="podman", architectures=architectures):
                            await buildpodman.manifest(architectures=architectures)
                case Namespace(
                    subcommand_container="login",
                ):
                    await login.subcommand(tool=parsedargs.tool)
        case Namespace(subcommand="product", subcommand_product="revision"):
            print(await produce_revision())  # noqa: T201
        case Namespace(subcommand="product", subcommand_product="provision"):
            await provision.subcommand(
                path_directory_product=await parsedargs.path_directory_product.absolute().resolve(),
                python=parsedargs.python,
                kubernetes=parsedargs.kubernetes,
            )
        case Namespace(subcommand="check", subcommand_check="jupyter"):
            await jupyter.subcommand(paths_notebook=parsedargs.paths_notebook)


def create_subcommand_check(argumentparser_check: ArgumentParser) -> ArgumentParser:
    subcommand_check = argumentparser_check.add_subparsers(
        dest="subcommand_check",
        help="Subcommand.",
    )
    subcommand_check_jupyter = subcommand_check.add_parser(
        name="jupyter",
        description=jupyter.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    subcommand_check_jupyter.add_argument(
        "paths_notebook",
        nargs="+",
        help="Path to a Jupyter notebook.",
    )
    return argumentparser_check


def create_subcommand_container(argumentparser_container: ArgumentParser) -> ArgumentParser:
    subcommand_container = argumentparser_container.add_subparsers(
        dest="subcommand_container",
        help="Subcommand.",
    )
    argumentparser_container_build = subcommand_container.add_parser(
        name="build",
        description=container.build.__doc__,
    )
    argumentparser_container_build.add_argument(
        "--tool",
        choices=("docker", "nerdctl", "podman"),
        help="Container image build tool.",
        required=True,
    )
    argumentparser_container_manifest = subcommand_container.add_parser(
        name="manifest",
        description=container.build.__doc__,
    )
    argumentparser_container_manifest.add_argument(
        "--tool",
        choices=("podman",),
        help="Container image manifest tool.",
        required=True,
    )
    argumentparser_container_manifest.add_argument(
        "architectures",
        nargs="+",
        help="Container image `--arch` value. See https://docs.podman.io/en/stable/markdown/podman-build.1.html#arch-arch.",
    )
    argumentparser_container_login = subcommand_container.add_parser(
        name="login",
        description=container.login.__doc__,
    )
    argumentparser_container_login.add_argument(
        "--tool",
        choices=("crane", "podman"),
        help="Container image registry login tool.",
        required=True,
    )
    return argumentparser_container


def create_subcommand_kubernetes(argumentparser_kubernetes: ArgumentParser) -> ArgumentParser:
    subcommand_kubernetes = argumentparser_kubernetes.add_subparsers(
        dest="subcommand_kubernetes",
        help="Subcommand.",
    )
    argumentparser_kubernetes_adapt_wsl = subcommand_kubernetes.add_parser(
        name="adapt_wsl",
        description=adapt_wsl.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    argumentparser_kubernetes_adapt_wsl.add_argument(
        "--path_dir_userprofile",
        help=r"Path to %USERPROFILE% directory.",
        required=True,
        type=Path,
    )
    argumentparser_kubernetes_adapt_wsl.add_argument(
        "--path_input",
        help="Path to adapt to WSL.",
        required=True,
        type=Path,
    )
    argumentparser_kubernetes_create_namespace = subcommand_kubernetes.add_parser(
        name="create_namespace",
        description=create_namespace.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    argumentparser_kubernetes_create_namespace.add_argument("--name", help="Namespace name.", required=True)
    _ = subcommand_kubernetes.add_parser(
        name="debug",
        description=debug.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    argumentparser_kubernetes_delete_namespace = subcommand_kubernetes.add_parser(
        name="delete_namespace",
        description=delete_namespace.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    argumentparser_kubernetes_delete_namespace.add_argument("--name", help="Namespace name.", required=True)
    argumentparser_kubernetes_recreate_basicauth = subcommand_kubernetes.add_parser(
        name="recreate_basicauth",
        description=recreate_basicauth.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    argumentparser_kubernetes_recreate_basicauth.add_argument("--namespace", help="Namespace name.", required=True)
    argumentparser_kubernetes_recreate_basicauth.add_argument("--name", help="Secret name.", required=True)
    argumentparser_kubernetes_recreate_basicauth.add_argument("--username", help="Basic auth username.", required=True)
    argumentparser_kubernetes_recreate_sshkeys = subcommand_kubernetes.add_parser(
        name="recreate_sshkeys",
        description=recreate_sshkeys.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    argumentparser_kubernetes_recreate_sshkeys.add_argument("--namespace", help="Namespace name.", required=True)
    argumentparser_kubernetes_run_module_under_wsl = subcommand_kubernetes.add_parser(
        name="run_module_under_wsl",
        description=run_module_under_wsl.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    argumentparser_kubernetes_run_module_under_wsl.add_argument(
        "--path_dir_working",
        help=r"Path to %USERPROFILE% directory.",
        required=True,
        type=Path,
    )
    argumentparser_kubernetes_run_module_under_wsl.add_argument(
        "--modulepath",
        help="Path to adapt to WSL.",
        required=True,
    )
    argumentparser_kubernetes_lifecycle = subcommand_kubernetes.add_parser(
        name="lifecycle",
        description=lifecycle.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    _ = argumentparser_kubernetes_lifecycle.add_argument(
        "--site",
        help="Kustomize site. Choices depend on the Kustomize layout under `path_dir_kustomize`.",
        required=True,
    )
    _ = argumentparser_kubernetes_lifecycle.add_argument(
        "path_dir_kustomize",
        help="Kustomize directory path. Conventionally under `k8s/*/`.",
        type=Path,
    )
    subcommand_kubernetes_lifecycle = argumentparser_kubernetes_lifecycle.add_subparsers(
        dest="subcommand_kubernetes_lifecycle",
        help="Subcommand.",
    )
    _ = subcommand_kubernetes_lifecycle.add_parser(
        name="disable_datatransfercapability",
        description=Lifecycle.disable_datatransfercapability.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    _ = subcommand_kubernetes_lifecycle.add_parser(
        name="enable_datatransfercapability",
        description=Lifecycle.enable_datatransfercapability.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    _ = subcommand_kubernetes_lifecycle.add_parser(
        name="deploy",
        description=Lifecycle.deploy.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    _ = subcommand_kubernetes_lifecycle.add_parser(
        name="destroy",
        description=Lifecycle.destroy.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    argumentparser_kubernetes_taloslinux = subcommand_kubernetes.add_parser(
        name="taloslinux",
        description=taloslinux.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    _ = argumentparser_kubernetes_taloslinux.add_argument(
        "path_dir_configuration",
        help="Path to root directory of configuration directory.",
        type=Path,
    )
    subcommand_kubernetes_taloslinux = argumentparser_kubernetes_taloslinux.add_subparsers(
        dest="subcommand_kubernetes_taloslinux",
        help="Subcommand.",
    )
    argumentparser_kubernetes_taloslinux.add_argument(
        "--generate_taloslinux_configuration",
        action=BooleanOptionalAction,
        default=False,
        help="Generate the Talos Linux configuration. Set to false to reuse files generated previously. ⚠️ Destructive "
        "if True.",
    )
    subcommand_kubernetes_taloslinux.add_parser(
        name="connect",
        description=Taloslinux.connect.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    subcommand_kubernetes_taloslinux.add_parser(
        name="deploy",
        description=Taloslinux.deploy.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    subcommand_kubernetes_taloslinux.add_parser(
        name="destroy",
        description=Taloslinux.destroy.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    return argumentparser_kubernetes


def create_subcommand_product(argumentparser_product: ArgumentParser) -> ArgumentParser:
    subcommand_product = argumentparser_product.add_subparsers(
        dest="subcommand_product",
        help="Subcommand.",
    )
    _ = subcommand_product.add_parser("revision")
    argumentparser_product_provision = subcommand_product.add_parser(
        name="provision",
        description=provision.__doc__,
        formatter_class=ArgumentDefaultsHelpFormatter,
    )
    argumentparser_product_provision.add_argument(
        "path_directory_product",
        help="Directory to create product in.",
        type=Path,
    )
    argumentparser_product_provision.add_argument(
        "--kubernetes",
        default=True,
        help="Will Git-based work product have Kubernetes affordances?",
        action=BooleanOptionalAction,
    )
    argumentparser_product_provision.add_argument(
        "--python",
        default=False,
        help="Will Git-based work product have Python affordances?",
        action=BooleanOptionalAction,
    )
    return argumentparser_product


if __name__ == "__main__":
    # TODO: Make log level configurable.
    run(main=main(), debug=True)
