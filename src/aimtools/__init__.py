"""The AIM Tools help with common, somewhat complicated system administration tasks in a cross-platform, standardized
way. The AIM Tools have a CLI and can also be used as a Python library."""

# Use an explicit re-export.
# See https://packaging.python.org/en/latest/discussions/versioning/#accessing-version-information-at-runtime
from aimtools.__version__ import __version__ as __version__  # noqa: PLC0414 # isort: skip
