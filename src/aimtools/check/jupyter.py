"""Checks if Jupyter notebooks are clean, meaning not containing metadata our output."""

from __future__ import annotations

from difflib import unified_diff
from io import BytesIO
from typing import TYPE_CHECKING

from anyio import Path

from aimtools.cli import execute

if TYPE_CHECKING:
    from collections.abc import Iterable


class NotebookNotCleanError(Exception):
    def __init__(self) -> None:
        super().__init__("Jupyter notebook contains metadata and/or output. ")


async def check_if_notebook_is_clean(path_notebook: Path) -> None:
    notebook_original = BytesIO((await path_notebook.read_text()).encode())
    notebook_new_bytesio = BytesIO()
    await execute(
        "jupyter",
        "nbconvert",
        "--ClearOutputPreprocessor.enabled=True",
        "--ClearMetadataPreprocessor.enabled=True",
        "--stdin",
        "--stdout",
        "--to",
        "notebook",
        stdin=notebook_original,
        stdout=notebook_new_bytesio,
        text=True,
    )
    if diff := "\n".join(
        unified_diff(
            a=notebook_original.getvalue().decode().splitlines(),
            b=notebook_new_bytesio.getvalue().decode().splitlines(),
            fromfile=f"{path_notebook} (before)",
            tofile=f"{path_notebook} (after)",
        ),
    ):
        print(diff)  # noqa: T201
        raise NotebookNotCleanError()


async def subcommand(paths_notebook: Iterable[Path]) -> None:
    for path_notebook in [Path(str_path_notebook) for str_path_notebook in paths_notebook]:
        await check_if_notebook_is_clean(path_notebook=path_notebook)
