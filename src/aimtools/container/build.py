"""Build and push container images."""

from __future__ import annotations

import sys
from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from errno import EINVAL
from itertools import chain
from os import SEEK_CUR, SEEK_END, cpu_count, environ, fsync
from platform import system
from tempfile import NamedTemporaryFile

from anyio import AsyncFile, Path, wrap_file
from loguru import logger

# The first condition is required for Mypy. See https://github.com/python/typing/issues/1732
if sys.platform.startswith("linux") and system() == "Linux":
    # Pylint can't be made to understand that this import is OS-dependent.
    # pylint: disable-next=no-name-in-module
    from os import sched_getaffinity

from typing import TYPE_CHECKING, ClassVar, cast

from aimtools.cli import execute

if TYPE_CHECKING:
    from collections.abc import Sequence


class ImageindexcouldntbeaccessedError(RuntimeError):
    def __init__(self) -> None:
        super().__init__("Image index could not be accessed.")


@dataclass(kw_only=True, slots=True)
# The attributes are essential and aren't futher coupling in dependency classes isn't so useful.
# pylint: disable-next=too-many-instance-attributes
class Build(ABC):
    architecture: str
    imagereferences: list[str] = field(init=False)
    imagereferences_manifest: list[str] = field(init=False)
    name_image: str
    name_product: str
    os: str
    path_dir_output: Path
    registry_image_product: str
    registry_image_base: str
    revision: str
    target: str

    EXITSTATUS_CRANE_IMAGEDOESNTEXIST: ClassVar[int] = 125

    def __post_init__(self) -> None:
        self.imagereferences = list(self.produce_imagereferences(architecture=self.architecture))
        self.imagereferences_manifest = list(self.produce_imagereferences(architecture=None))

    def produce_imagereferences(self, *, architecture: str | None) -> Sequence[str]:
        architecture_str = f"{architecture}--" if architecture else ""
        return [
            f"{self.registry_image_product}{self.name_image}:{architecture_str}{self.target}",
            f"{self.registry_image_product}{self.name_image}:{architecture_str}{self.target}--{self.revision}",
        ]

    @abstractmethod
    def _produce_command_push(self) -> Sequence[str]: ...

    def _produce_command_build(self, *, path_file_imageid: Path) -> Sequence[str]:
        path_file_containerfile = Path("k8s") / self.name_product / "Containerfile"
        # TODO: (security) Sanitize env var values.
        return [
            # Set for reproducibility.
            "--build-arg",
            "SOURCE_DATE_EPOCH=1732563483",
            "--build-arg",
            f"NAME_PRODUCT={self.name_product}",
            "--build-arg",
            f"REGISTRY_IMAGE_BASE={self.registry_image_base}",
            "--build-arg",
            f"REGISTRY_IMAGE_PRODUCT={self.registry_image_product}",
            "--build-arg",
            f"REVISION={self.revision}",
            f"--file={path_file_containerfile!s}",
            f"--platform={self.os}/{self.architecture}",
            f"--iidfile={path_file_imageid!s}",
            "--progress=plain",
            "--squash",
            *[
                argument
                for imagereference in chain(
                    self.produce_imagereferences(architecture=self.architecture),
                    self.imagereferences_manifest,
                )
                for argument in ("--tag", imagereference)
            ],
            "--target",
            self.target,
            ".",
        ]

    async def run(self) -> None:
        logger.debug("{}", str(self))
        with NamedTemporaryFile(
            delete=False,
            dir=self.path_dir_output,
            mode="x+b",
            prefix="imageid",
            suffix=".txt",
        ) as file_imageid:
            logger.info(
                "run()",
            )
            _ = await execute(
                *self._produce_command_build(path_file_imageid=Path(file_imageid.name)),
                stderr=logger,
                stdout=logger,
                text=True,
            )
            file_imageid.flush()
            fsync(file_imageid)
            file_imageid.seek(0)
            # TODO: Make configurable.
            # Only push with access to the registry, e.g., under CI/CD.
            if "CI_REGISTRY" in environ:
                await self._push(file_imageid=wrap_file(file_imageid))

    async def _push(self, *, file_imageid: AsyncFile[bytes]) -> None:
        # TODO: Make cross-builder.
        async with file_imageid:
            imageid = (await _read_last_line(file=file_imageid)).decode().rstrip()
            for imagereference in self.imagereferences:
                await execute(
                    *self._produce_command_push(),
                    "image",
                    "push",
                    "--compression-format=zstd:chunked",
                    imageid,
                    imagereference,
                    stderr=logger,
                    stdout=logger,
                    text=True,
                )

    async def manifest(self, *, architectures: Sequence[str]) -> None:
        name_manifest = f"manifest-{self.name_image}:{self.architecture}--{self.target}--{self.revision}"
        # TODO: Handle existing manifest.
        _ = await execute(
            "podman",
            "manifest",
            "create",
            name_manifest,
            stderr=logger,
            stdout=logger,
            text=True,
        )
        for architecture in architectures:
            for imagereference in self.produce_imagereferences(architecture=architecture):
                _ = await execute(
                    "podman",
                    "manifest",
                    "add",
                    name_manifest,
                    imagereference,
                    stderr=logger,
                    stdout=logger,
                    text=True,
                )
        for imagereference in self.imagereferences_manifest:
            _ = await execute(
                "podman",
                "manifest",
                "push",
                "--all",
                name_manifest,
                f"docker://{imagereference}",
                stderr=logger,
                stdout=logger,
                text=True,
            )
        _ = await execute(
            "podman",
            "manifest",
            "rm",
            name_manifest,
            stderr=logger,
            stdout=logger,
            text=True,
        )


@dataclass(kw_only=True, slots=True)
class BuildDocker(Build):
    _COMMAND: ClassVar[str] = "docker"

    def _produce_command_push(self) -> Sequence[str]:
        return [self._COMMAND]

    def _produce_command_build(self, *, path_file_imageid: Path) -> Sequence[str]:
        return [
            self._COMMAND,
            "--debug",
            "--load",
            "buildx",
            "build",
            *(super(BuildDocker, self)._produce_command_build(path_file_imageid=path_file_imageid)),
        ]


@dataclass(kw_only=True, slots=True)
class BuildNerdctl(Build):
    _COMMAND: ClassVar[str] = "nerdctl"

    def _produce_command_push(self) -> Sequence[str]:
        return [self._COMMAND]

    def _produce_command_build(self, *, path_file_imageid: Path) -> Sequence[str]:
        return [
            self._COMMAND,
            "--debug-full",
            "--namespace",
            "k8s.io",
            "build",
            *(super(BuildNerdctl, self)._produce_command_build(path_file_imageid=path_file_imageid)),
        ]


@dataclass(kw_only=True, slots=True)
class BuildPodman(Build):
    _COMMAND: ClassVar[str] = "podman"

    def _produce_command_push(self) -> Sequence[str]:
        return [self._COMMAND, "--log-level=debug"]

    def _produce_command_build(
        self,
        *,
        path_file_imageid: Path,
    ) -> Sequence[str]:
        # Conditional can't be expression. See https://github.com/python/typing/issues/1732
        if sys.platform.startswith("linux"):
            cpucount = len(sched_getaffinity(0))
        else:
            cpucount = cpu_count()
        return [
            self._COMMAND,
            "--log-level=debug",
            "buildx",
            "build",
            f"--arch={self.architecture}",
            "--build-arg",
            "SELINUXRELABEL=,z",
            f"--jobs={cpucount}",
            f"--os={self.os}",
            "--timestamp=0",
            *[
                token
                for token in (
                    super(BuildPodman, self)._produce_command_build(
                        path_file_imageid=path_file_imageid,
                    )
                )
                if not token.startswith("--platform=")
            ],
        ]


async def _read_last_line(
    *,
    file: AsyncFile[bytes],
    separator: bytes = b"\n",
    skip_final_separator: bool = True,
) -> bytes:
    buffer = bytearray(len(separator))
    try:
        position = await file.seek(-len(buffer), SEEK_END)
    except OSError as exception:
        if exception.errno != EINVAL:
            raise
        return b""
    if skip_final_separator and position:
        position = await file.seek(-1, SEEK_CUR)
    if not position:
        if not skip_final_separator:
            _ = await file.seek(1, SEEK_CUR)
        return await file.read()
    # pylint: disable-next=while-used
    while buffer != separator and position >= len(buffer):
        # Cast as work-around for https://github.com/agronholm/anyio/issues/825.
        size = cast(int, await file.readinto(buffer))
        if buffer == separator:
            break
        position = await file.seek(-size - 1, SEEK_CUR)
    return await file.read()
