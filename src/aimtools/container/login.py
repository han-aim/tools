"""Log in to container image registries."""

from getpass import getpass
from io import BytesIO
from os import environ
from sys import stderr
from urllib.parse import urlparse

from loguru import logger

from aimtools.cli import execute


async def login_containerregistry(username: str, password: BytesIO, registry: str, tool: str) -> None:
    match tool:
        case "podman":
            await execute(
                "podman",
                "--log-level=debug",
                "login",
                "--username",
                username,
                "--password-stdin",
                registry,
                stdin=password,
                stderr=logger,
                stdout=logger,
                text=True,
            )
        case "crane":
            if registry.startswith(("https://", "http://", "//")) and (registry := urlparse(registry).netloc) is None:
                message = f"{registry} is not a usable URL."
                raise ValueError(message)
            await execute(
                "crane",
                "auth",
                "login",
                "--username",
                username,
                "--password-stdin",
                # Work around https://github.com/google/go-containerregistry/issues/766.
                registry,
                stdin=password,
                stderr=logger,
                stdout=logger,
                text=True,
            )
        case _:
            raise NotImplementedError()


async def subcommand(tool: str) -> None:
    if "CI_DEPENDENCY_PROXY_USER" in environ:
        await login_containerregistry(
            username=environ["CI_DEPENDENCY_PROXY_USER"],
            password=BytesIO(environ["CI_DEPENDENCY_PROXY_PASSWORD"].encode()),
            registry=environ["CI_DEPENDENCY_PROXY_SERVER"],
            tool=tool,
        )
    if "CI_REGISTRY_USER" in environ:
        await login_containerregistry(
            username=environ["CI_REGISTRY_USER"],
            password=BytesIO(environ["CI_REGISTRY_PASSWORD"].encode()),
            registry=environ["CI_REGISTRY"],
            tool=tool,
        )
    else:
        print(  # noqa: T201
            f"Please provide credentials to the container image registry {environ['REGISTRY']} ...",
            file=stderr,
            flush=True,
        )
        await login_containerregistry(
            username=input("Username: "),
            password=BytesIO(getpass().encode()),
            registry=environ["REGISTRY"],
            tool=tool,
        )
