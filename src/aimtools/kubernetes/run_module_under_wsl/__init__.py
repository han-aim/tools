"""Run this Python `__main__` module under WSL2."""

from __future__ import annotations

from io import BytesIO
from os import chdir
from runpy import run_module

from anyio import Path
from loguru import logger

from aimtools.cli import execute

__deprecated__ = "This is no longer in use without Rancher Desktop."


async def subcommand(path_dir_working: Path, modulepath: str) -> None:
    bytesio_stdout = BytesIO()
    _ = await execute(
        "wslpath",
        "-a",
        str(path_dir_working),
        stderr=logger,
        stdout=bytesio_stdout,
        text=True,
    )
    path_dir_working = Path(
        bytesio_stdout.getvalue().decode().rstrip("\n"),
    )
    chdir(path_dir_working)
    run_module(modulepath, {}, "__main__")
