"""Add a host configuration to the OpenSSH client configuration."""

from __future__ import annotations

from base64 import b64decode
from contextlib import suppress
from ipaddress import IPv4Address, IPv6Address
from re import compile as re_compile
from typing import Literal, assert_never

from anyio import Path
from kubernetes import client
from loguru import logger

from aimtools.kubernetes import load_kubeconfig

_REGEXPATTERN_OPENSSHCLIENTHOST = "^[a-zA-Z0-9_-]+$"
_REGEX_OPENSSHCLIENTHOST = re_compile(_REGEXPATTERN_OPENSSHCLIENTHOST)


# TODO: Rewrite as `dataclass``.
# Allow PLR0913 for now in anticipation of previously listed TODOs.
async def configure_ssh(  # noqa: PLR0913
    *,
    application: Literal["rrsync", "taloslinux-tunnel"],
    endpoint: IPv4Address | IPv6Address,
    name_user: str,
    namespace: str | None = None,
    opensshclient_host: str,
    opensshclient_hostname: str,
    path_dir_config_included: Path,
) -> None:
    path_dir_ssh = await Path.home() / ".ssh"
    await path_dir_ssh.mkdir(mode=0o700, exist_ok=True)
    path_file_sshconfiguration_main = path_dir_ssh / "config"
    endpoint_str = f"{f"[{endpoint}]" if isinstance(endpoint, IPv6Address) else str(endpoint)}"
    if not _REGEX_OPENSSHCLIENTHOST.match(opensshclient_host):
        msg = (
            f"`{opensshclient_host}` is not a hostname that matches the restricted regular expression"
            f"`{_REGEXPATTERN_OPENSSHCLIENTHOST}`"
        )
        raise ValueError(msg)
    if any(token in str(path_dir_config_included) for token in ("%", "~")):
        msg = (
            f"`{path_dir_config_included!s}` was roughly sanitized and possibly contains special OpenSSH configuration "
            "tokens (https://man.openbsd.org/ssh_config#TOKENS)`. To avoid security vulnerabilities, please use a more "
            "conventional path."
        )
        raise ValueError(msg)
    match application:
        case "rrsync":
            load_kubeconfig()
            corev1api = client.CoreV1Api()
            if not namespace:
                message = "A Kubernetes namespace (`namespace` parameter) must be specified for `rrsync`."
                raise ValueError(message)
            path_file_sshknownhosts = path_dir_ssh / f"known_hosts_rrsync_{opensshclient_host}"
            if await path_file_sshknownhosts.exists():
                await path_file_sshknownhosts.unlink()
            path_file_sshprivatekey = path_dir_ssh / f"private_key_rrsync_{opensshclient_host}"
            path_file_sshconfiguration_main = path_dir_ssh / "config"
            secret = corev1api.read_namespaced_secret("identity-sshd", namespace)
            sshprivatekey = b64decode(secret.data["ssh-privatekey"], validate=True).decode(encoding="ascii")
            await path_file_sshprivatekey.touch(0o600)
            await path_file_sshprivatekey.write_text(sshprivatekey, encoding="ascii")
            opensshclient_configurationblock = f"""Host rrsync-{opensshclient_host}\n
    HostName {opensshclient_hostname!s}
    IdentityFile {path_file_sshprivatekey}
    Port 2222
    RequestTTY no
    StrictHostKeyChecking no
    User {name_user}
    UserKnownHostsFile {path_file_sshknownhosts!s}
    VisualHostKey yes"""
        case "taloslinux-tunnel":
            opensshclient_configurationblock = f"""HostName {opensshclient_hostname!s}
    IdentitiesOnly yes
    IdentityFile ${{AIMTOOLS_PATH_FILE_OPENSSHCLIENT_PRIVATEKEY}}
    User root
    UserKnownHostsFile ${{AIMTOOLS_PATH_FILE_OPENSSHCLIENT_KNOWNHOSTS}}"""
            opensshclient_configurationblock_proxy = f"""Host {opensshclient_host}-proxy
    {opensshclient_configurationblock}
    ExitOnForwardFailure yes
    LocalForward {endpoint_str!s}:50000 /run/{opensshclient_host}/apid.sock
    LocalForward {endpoint_str!s}:6443 /run/{opensshclient_host}/kube-apiserver.sock
    RequestTTY no
    SessionType none
    StdinNull yes"""
            opensshclient_configurationblock = f"""Host {opensshclient_host}
    {opensshclient_configurationblock}

{opensshclient_configurationblock_proxy}"""
        case _:
            assert_never(application)
    path_file_sshconfiguration_include = await (path_dir_config_included / f"config_{opensshclient_host}").resolve()
    logger.debug("Writing OpenSSH client configuration to '{!s}' ...", path_file_sshconfiguration_include)
    await path_file_sshconfiguration_include.write_text(opensshclient_configurationblock, encoding="utf-8")
    if not await path_file_sshconfiguration_main.exists():
        await path_file_sshconfiguration_main.touch()
    with suppress(ValueError):
        # Try to make the path relative to the user's home directory, in case their home directory changes path.
        path_file_sshconfiguration_include = "~" / path_file_sshconfiguration_include.relative_to(await Path.home())
    logger.debug("Including OpenSSH client configuration in '{!s}' ...", path_file_sshconfiguration_main)
    async with await path_file_sshconfiguration_main.open(mode="r+", encoding="utf-8") as file_sshconfiguration:
        content = await file_sshconfiguration.read()
        await file_sshconfiguration.seek(0, 0)
        directive_include = f"Include {path_file_sshconfiguration_include}\n\n"
        await file_sshconfiguration.write(directive_include + content if directive_include not in content else content)
