"""Tools to work with Kubernetes."""

from __future__ import annotations

from json import loads
from os import environ

from kubernetes import client, config
from kubernetes.client.exceptions import ApiException


def load_kubeconfig() -> None:
    kubeconfig = environ.get("KUBECONFIG")
    config.load_kube_config(config_file=kubeconfig, persist_config=False)


def create_namespaced_secret(
    corev1api: client.CoreV1Api,
    namespace: str,
    name: str,
    secret_key: client.V1Secret,
) -> None:
    try:
        corev1api.create_namespaced_secret(namespace=namespace, body=secret_key)
    except ApiException as fault:
        exception_unpacked = loads(fault.body)
        if exception_unpacked["reason"] == "AlreadyExists":
            corev1api.delete_namespaced_secret(name=name, namespace=namespace)
            corev1api.create_namespaced_secret(namespace=namespace, body=secret_key)
        else:
            raise
