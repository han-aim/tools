"""Write debugging information about the current Kubernetes cluster into a directory."""

from __future__ import annotations

from typing import TYPE_CHECKING

from loguru import logger

from aimtools.cli import execute

if TYPE_CHECKING:
    from anyio import Path


async def subcommand(path_dir_output: Path) -> None:
    logger.info("Writing debugging info about the current Kubernetes cluster into '{!s}' ...", path_dir_output)
    await execute(
        "kubectl",
        "cluster-info",
        "dump",
        "--all-namespaces=true",
        f"--output-directory={path_dir_output!s}",
        text=True,
    )
