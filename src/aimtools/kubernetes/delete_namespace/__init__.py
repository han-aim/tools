"""Delete a namespace."""

from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from kubernetes.client import CoreV1Api


def subcommand(corev1api: CoreV1Api, namespace: str) -> None:
    corev1api.delete_namespace(name=namespace)
