"""Manage application deployment instances."""

from __future__ import annotations

from dataclasses import dataclass
from re import Pattern
from re import compile as re_compile
from typing import TYPE_CHECKING, ClassVar

from loguru import logger

if TYPE_CHECKING:
    from anyio import Path

from aimtools.cli import execute


@dataclass(frozen=True, kw_only=True, slots=True)
# The attributes are essential and aren't futher coupling in dependency classes isn't so useful.
# pylint: disable-next=too-many-instance-attributes
class Lifecycle:
    site: str
    path_dir_kustomize: Path

    _REGEXPATTERN_SITE: ClassVar[str] = "^[a-zA-Z0-9_-]+$"
    _REGEX_SITE: ClassVar[Pattern[str]] = re_compile(_REGEXPATTERN_SITE)

    def __post_init__(self) -> None:
        if not Lifecycle._REGEX_SITE.match(self.site):
            message = f"`{self.site}` doesn't match the regular expression `{Lifecycle._REGEXPATTERN_SITE}`."
            raise ValueError(message)

    async def deploy(self) -> None:
        """Deploy as a new instance."""
        await execute(
            "kubectl",
            "apply",
            "--validate=strict",
            f"--kustomize={self.path_dir_kustomize / 'overlays' / self.site}",
            stderr=logger,
            stdout=logger,
            text=True,
        )

    async def destroy(self) -> None:
        """Destroy the deployment instance in the cluster. ⚠️ Destructive."""
        await execute(
            "kubectl",
            "delete",
            f"--kustomize={self.path_dir_kustomize / 'overlays' / self.site}",
            stderr=logger,
            stdout=logger,
            text=True,
        )

    async def enable_datatransfercapability(self) -> None:
        """Modify the deployment instance to enable data transfer capability (using `rrsync`)."""
        command_datatransfercapability_enable = [
            "kubectl",
            "apply",
            "--validate=strict",
            f"--kustomize={self.path_dir_kustomize / 'overlays' / 'rrsync'}",
        ]
        await execute(*command_datatransfercapability_enable, stderr=logger, stdout=logger, text=True)

    async def disable_datatransfercapability(self) -> None:
        """Modify the deployment instance to disable data transfer capability (using `rrsync`)."""
        command_datatransfercapability_disable = [
            "kubectl",
            "delete",
            f"--kustomize={self.path_dir_kustomize / 'overlays' / 'rrsync'}",
        ]
        await execute(*command_datatransfercapability_disable, stderr=logger, stdout=logger, text=True)


async def subcommand(site: str, path_dir_kustomize: Path, subcommand_kubernetes_lifecycle: str) -> Lifecycle:
    lifecycle = Lifecycle(site=site, path_dir_kustomize=path_dir_kustomize)
    logger.debug("Performing subcommand `{}` on {}.", subcommand_kubernetes_lifecycle, lifecycle)
    match subcommand_kubernetes_lifecycle:
        case "deploy":
            await lifecycle.deploy()
        case "destroy":
            await lifecycle.destroy()
        case "enable_datatransfercapability":
            await lifecycle.enable_datatransfercapability()
        case "disable_datatransfercapability":
            await lifecycle.disable_datatransfercapability()
    return lifecycle
