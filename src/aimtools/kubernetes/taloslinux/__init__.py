"""Manage Talos Linux based Kubernetes clusters."""

from __future__ import annotations

from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from ipaddress import (
    AddressValueError,
    IPv4Address,
    IPv4Interface,
    IPv4Network,
    IPv6Address,
    IPv6Interface,
    IPv6Network,
)
from shutil import copy, move
from typing import TYPE_CHECKING, Any, ClassVar

from anyio import AsyncFile, Path
from loguru import logger
from ruamel.yaml import YAML

from aimtools.cli import execute

if TYPE_CHECKING:
    from collections.abc import Collection, Generator, Iterator, Sequence


@dataclass(kw_only=True, slots=True)
class ConfigKubernetes:
    ipam_k8s: IpamK8s
    ipnetworks_k8s_pods: Sequence[IPv4Network | IPv6Network] = field(init=False)
    ipnetworks_k8s_services: Sequence[IPv4Network | IPv6Network] = field(init=False)
    # Blank (`""`) means ‘all interfaces and IP address families’ but seems to crash `kube-controller-manager`.
    # `"0.0.0.0"` means the same.
    ipaddress_bind_controlplane_k8s: None | str = None
    name_cluster: str
    version: str


@dataclass(kw_only=True, slots=True)
# The attributes are essential and aren't amenable to futher coupling in dependency classes.
# pylint: disable-next=too-many-instance-attributes
class ConfigInfra:
    # IPv6 subnets should be split to < /64 in general.
    # IPv4 subnets preferably should be split to < /25.
    IPAMNODESNONSEGMENTED_PREFIXLEN_MAX_IPV4NETWORK_NODES: ClassVar[int] = 25
    IPAMNODESNONSEGMENTED_PREFIXLEN_MAX_IPV6NETWORK_NODES: ClassVar[int] = 64
    N_NODES_CONTROLPLANE_QUORUM: ClassVar[int] = 3
    # TODO: Rename.
    hostname_endpoint_hypervisor: str
    ipaddress_endpoint_k8sapiserver_workstation: IPv4Address
    opensshclient_host: str
    opensshclient_port: int = 22
    ipam_nodes: IpamNodes
    ipinterface_hypervisor_ipv4: IPv4Interface
    ipinterface_hypervisor_ipv6: IPv6Interface
    ipnetworks_nodes_controlplane: Sequence[IPv4Network | IPv6Network] = field(init=False)
    ipnetworks_nodes_worker: Sequence[IPv4Network | IPv6Network] = field(init=False)
    n_nodes_controlplane: int = 3
    n_nodes_worker: int
    name_networkinterface_parent: str
    networkinterfaces_nodes_controlplane: list[list[NetworkInterface]] = field(init=False)
    networkinterfaces_nodes_worker: list[list[NetworkInterface]] = field(init=False)
    networkinterfaces_nodes: list[list[NetworkInterface]] = field(init=False)
    vlanid: int

    def __post_init__(self) -> None:
        assert self.n_nodes_controlplane >= self.N_NODES_CONTROLPLANE_QUORUM
        assert self.n_nodes_worker >= 1
        match self.ipam_nodes:
            case IpamNodesSegmented(
                exclusions=exclusions,
                ipnetworks_nodes_controlplane=ipnetworks_nodes_controlplane,
                ipnetworks_nodes_worker=ipnetworks_nodes_worker,
            ):
                self.ipnetworks_nodes_controlplane = list(ipnetworks_nodes_controlplane)
                self.ipnetworks_nodes_worker = list(ipnetworks_nodes_worker)
                for ipnetwork_nodes_worker in self.ipnetworks_nodes_worker:
                    for ipnetwork_nodes_controlplane in self.ipnetworks_nodes_controlplane:
                        assert not ipnetwork_nodes_controlplane.overlaps(ipnetwork_nodes_worker)
            case IpamNodesNonsegmented(exclusions=exclusions, ipnetworks_nodes=ipnetworks_nodes):
                self.ipnetworks_nodes_controlplane = []
                self.ipnetworks_nodes_worker = []
                for ipnetwork_nodes in ipnetworks_nodes:
                    if isinstance(ipnetwork_nodes, IPv4Network):
                        assert ipnetwork_nodes.prefixlen < self.IPAMNODESNONSEGMENTED_PREFIXLEN_MAX_IPV4NETWORK_NODES
                    else:
                        # To avoid splitting up to subnets smaller > /64, enforce /63.
                        assert ipnetwork_nodes.prefixlen < self.IPAMNODESNONSEGMENTED_PREFIXLEN_MAX_IPV6NETWORK_NODES
                    (ipnetwork_nodes_controlplane_extra, ipnetwork_nodes_worker_extra) = ipnetwork_nodes.subnets()
                    self.ipnetworks_nodes_controlplane.append(ipnetwork_nodes_controlplane_extra)
                    self.ipnetworks_nodes_worker.append(ipnetwork_nodes_worker_extra)
        self.networkinterfaces_nodes_controlplane = ConfigInfra._enumerate_networkinterfaces_for_hosts(
            exclusions={
                *exclusions,
            },
            index_node_max=self.n_nodes_controlplane,
            ipnetworks=self.ipnetworks_nodes_controlplane,
        )
        self.networkinterfaces_nodes_worker = ConfigInfra._enumerate_networkinterfaces_for_hosts(
            exclusions={
                *exclusions,
            },
            index_node_max=self.n_nodes_worker,
            ipnetworks=self.ipnetworks_nodes_worker,
        )
        self.networkinterfaces_nodes = [
            *self.networkinterfaces_nodes_controlplane,
            *self.networkinterfaces_nodes_worker,
        ]

    @classmethod
    def _enumerate_networkinterfaces_for_hosts(
        cls,
        exclusions: Collection[IPv4Address | IPv6Address],
        index_node_max: int,
        ipnetworks: Sequence[IPv4Network | IPv6Network],
    ) -> list[list[NetworkInterface]]:
        """Returns a list, with one list of suitable network interfaces for hosts per `ipnetwork`."""
        assert index_node_max > 0
        networkinterfaces_nodes: list[list[NetworkInterface]] = [[] for _ in range(index_node_max)]
        for ipnetwork in ipnetworks:
            index_node = 0
            # Convention: first host is `ipnetwork` is the gateway.
            gateway: IPv4Address | IPv6Address = (
                IPv6Address(next(ipnetwork.hosts()))
                if isinstance(ipnetwork, IPv6Network)
                else IPv4Address(next(ipnetwork.hosts()))
            )
            for ipaddress_host in ipnetwork.hosts():
                if ipaddress_host not in exclusions and ipaddress_host != gateway:
                    ipinterface_str = f"{ipaddress_host}/{ipnetwork.prefixlen}"
                    networkinterfaces_nodes[index_node].append(
                        NetworkInterface(
                            gateway=gateway,
                            ipinterface=(
                                IPv6Interface(ipinterface_str)
                                if isinstance(ipnetwork, IPv6Network)
                                else IPv4Interface(ipinterface_str)
                            ),
                        ),
                    )
                    index_node += 1
                    if index_node >= index_node_max:
                        break
            if index_node < index_node_max:
                message = (
                    f"Insufficient ({index_node} instead of {index_node_max}) suitable IP interfaces found in subnets."
                )
                raise ValueError(message)
        return networkinterfaces_nodes

    def describe_node(self, index: int) -> tuple[bool, str]:
        is_controlplane = index < self.n_nodes_controlplane
        return is_controlplane, f"{'controlplane' if is_controlplane else 'worker'}-{index}"


@dataclass(kw_only=True, slots=True)
class ConfigTaloslinux:
    cuda: bool = False
    dnsresolvers: Sequence[IPv4Address | IPv6Address]
    enable_discovery: bool = field(init=False)
    enable_kubespan: bool = field(init=False)
    generate_taloslinux_configuration: bool
    loadbalancing_nodes: None | LoadbalancingNodes
    version: str

    def __post_init__(self) -> None:
        match self.loadbalancing_nodes:
            case LoadbalancingNodesL7():
                self.enable_discovery = True
                self.enable_kubespan = True
            case _:
                self.enable_discovery = False
                self.enable_kubespan = False


@dataclass(frozen=True, kw_only=True, slots=True)
class NetworkInterface:
    gateway: IPv4Address | IPv6Address
    ipinterface: IPv4Interface | IPv6Interface


class IpamNodesNonsegmentedIpnetworksNodesEmptyError(ValueError):
    def __init__(self) -> None:
        super().__init__("`ipnetworks_nodes` cannot be empty.")


@dataclass(frozen=True, kw_only=True, slots=True)
class IpamNodesNonsegmented:
    """`exclusions`: extra exclusions for node IP interface enumeration,
    in addition to the `ipnetwork`'s gateway."""

    exclusions: Collection[IPv4Address | IPv6Address] = field(default_factory=list)
    ipnetworks_nodes: Sequence[IPv4Network | IPv6Network]

    def __post_init__(self) -> None:
        if len(self.ipnetworks_nodes) < 1:
            raise IpamNodesNonsegmentedIpnetworksNodesEmptyError()


class IpamNodesSegmentedIpnetworksNodesWorkerEmptyError(ValueError):
    def __init__(self) -> None:
        super().__init__("`ipnetworks_nodes_worker` cannot be empty.")


class IpamNodesSegmentedIpnetworksNodesControlplaneEmptyError(ValueError):
    def __init__(self) -> None:
        super().__init__("`ipnetworks_nodes_controlplane` cannot be empty.")


@dataclass(frozen=True, kw_only=True, slots=True)
class IpamNodesSegmented:
    """`exclusions`: extra exclusions for node IP interface enumeration,
    in addition to the `ipnetwork`'s gateway."""

    exclusions: Collection[IPv4Address | IPv6Address] = field(default_factory=list)
    ipnetworks_nodes_controlplane: Sequence[IPv4Network | IPv6Network]
    ipnetworks_nodes_worker: Sequence[IPv4Network | IPv6Network]

    def __post_init__(self) -> None:
        if len(self.ipnetworks_nodes_controlplane) < 1:
            raise IpamNodesSegmentedIpnetworksNodesControlplaneEmptyError()
        if len(self.ipnetworks_nodes_worker) < 1:
            raise IpamNodesSegmentedIpnetworksNodesWorkerEmptyError()


IpamNodes = IpamNodesNonsegmented | IpamNodesSegmented


class IpamK8sNonsegmentedIpnetworksK8sEmptyError(ValueError):
    def __init__(self) -> None:
        super().__init__("`ipnetworks_k8s` cannot be empty.")


@dataclass(frozen=True, kw_only=True, slots=True)
class IpamK8sNonsegmented:
    ipnetworks_k8s: Sequence[IPv4Network | IPv6Network]

    def __post_init__(self) -> None:
        if len(self.ipnetworks_k8s) < 1:
            raise IpamK8sNonsegmentedIpnetworksK8sEmptyError()


class IpamK8sSegmentedIpnetworksK8sPodsEmptyError(ValueError):
    def __init__(self) -> None:
        super().__init__("`ipnetworks_k8s_pods` cannot be empty.")


class IpamK8sSegmentedIpnetworksK8sServicesEmptyError(ValueError):
    def __init__(self) -> None:
        super().__init__("`ipnetworks_k8s_services` cannot be empty.")


@dataclass(frozen=True, kw_only=True, slots=True)
class IpamK8sSegmented:
    ipnetworks_k8s_pods: Sequence[IPv4Network | IPv6Network]
    ipnetworks_k8s_services: Sequence[IPv4Network | IPv6Network]

    def __post_init__(self) -> None:
        if len(self.ipnetworks_k8s_pods) < 1:
            raise IpamK8sSegmentedIpnetworksK8sPodsEmptyError()
        if len(self.ipnetworks_k8s_services) < 1:
            raise IpamK8sSegmentedIpnetworksK8sServicesEmptyError()


# How to perform IP address management inside Kubernetes.
IpamK8s = IpamK8sNonsegmented | IpamK8sSegmented


@dataclass(frozen=True, kw_only=True, slots=True)
class LoadbalancingNodesL4:
    """IP forwarding or some other OSI L4 method of loadbalancing

    Set to masquerade IP interface."""

    ipinterface_loadbalancer_nodes: IPv4Interface | IPv6Interface


@dataclass(frozen=True, kw_only=True, slots=True)
class LoadbalancingNodesL7:
    """HTTP reverse proxy or some other OSI L7 method of loadbalancing

    Set to loadbalancer's IP interface."""

    ipinterface_loadbalancer_nodes: IPv4Interface | IPv6Interface


# How load balancing on the nodes is done (for access to control plane components `apid` and `kube-apiserver` on all
# control plane nodes).
LoadbalancingNodes = LoadbalancingNodesL4 | LoadbalancingNodesL7


if TYPE_CHECKING:
    from collections.abc import Sequence


# The attributes are essential and aren't amenable to futher coupling in dependency classes.
# pylint: disable-next=too-many-instance-attributes
class Taloslinux(ABC):

    # A Kubernetes architectural restriction on service subnet sizes.
    IPAMK8SSEGMENTED_IPV6NETWORK_K8S_SERVICES_PREFIXLEN_MIN: ClassVar[int] = 108
    IPAMK8SSEGMENTED_IPV4NETWORK_K8S_SERVICES_PREFIXLEN_MIN: ClassVar[int] = 20
    IPAMK8SNONSEGMENTED_IPV6NETWORK_K8S_SERVICES_PREFIXLEN_MIN: ClassVar[int] = 107
    IPAMK8SNONSEGMENTED_IPV4NETWORK_K8S_SERVICES_PREFIXLEN_MIN: ClassVar[int] = 19

    def _produce_subnets(
        self,
    ) -> Iterator[list[IPv4Network | IPv6Network]]:
        """
        Returns:
        Pairs of CIDR subnets, corresponding to Kubernetes CLI parameters `--pod-network-cidr`, `--service-cidr`.
        """
        # TODO: Azure misuses the Subnet-Router anycast address as unicast, so .hosts() should not be used since
        # it excludes that address.
        # https://networkengineering.stackexchange.com/questions/63563/is-the-first-address-in-an-ipv6-subnet-assignable
        match self.configkubernetes.ipam_k8s:
            case IpamK8sSegmented(
                ipnetworks_k8s_pods=ipnetworks_k8s_pods,
                ipnetworks_k8s_services=ipnetworks_k8s_services,
            ):
                for ipnetwork_k8s_pods in ipnetworks_k8s_pods:
                    assert ipnetwork_k8s_pods.is_private
                    for ipnetwork_k8s_services in ipnetworks_k8s_services:
                        assert ipnetwork_k8s_services.is_private
                        assert (
                            ipnetwork_k8s_services.prefixlen
                            >= Taloslinux.IPAMK8SSEGMENTED_IPV6NETWORK_K8S_SERVICES_PREFIXLEN_MIN
                            if isinstance(ipnetwork_k8s_services, IPv6Network)
                            else ipnetwork_k8s_services.prefixlen
                            >= Taloslinux.IPAMK8SSEGMENTED_IPV4NETWORK_K8S_SERVICES_PREFIXLEN_MIN
                        )
                        assert not ipnetwork_k8s_pods.overlaps(ipnetwork_k8s_services)
                        # These ranges must be exclusive.
                        for networkinterfaces_node in self.configinfra.networkinterfaces_nodes:
                            # TODO: Use exceptions instead of assertions.
                            for networkinterface_node in networkinterfaces_node:
                                assert not ipnetwork_k8s_pods.overlaps(networkinterface_node.ipinterface.network)
                                assert not ipnetwork_k8s_services.overlaps(networkinterface_node.ipinterface.network)
                yield from zip(ipnetworks_k8s_pods, ipnetworks_k8s_services, strict=True)
            case IpamK8sNonsegmented(ipnetworks_k8s=ipnetworks_k8s):
                assert ipnetworks_k8s is not None
                for ipnetwork_k8s in ipnetworks_k8s:
                    assert ipnetwork_k8s.is_private
                    # TODO: Test.
                    assert (
                        ipnetwork_k8s.prefixlen >= self.IPAMK8SNONSEGMENTED_IPV6NETWORK_K8S_SERVICES_PREFIXLEN_MIN
                        if isinstance(ipnetwork_k8s, IPv6Network)
                        else ipnetwork_k8s.prefixlen >= self.IPAMK8SNONSEGMENTED_IPV4NETWORK_K8S_SERVICES_PREFIXLEN_MIN
                    )
                    # These ranges must be exclusive.
                    for networkinterfaces_node in self.configinfra.networkinterfaces_nodes:
                        for networkinterface_node in networkinterfaces_node:
                            assert not ipnetwork_k8s.overlaps(networkinterface_node.ipinterface.network)
                    # Temporary variable required for Mypy type inference.
                    ipnetworks_k8s_subnets: Generator[list[IPv4Network | IPv6Network], None, None] = (
                        list(ipnetwork_k8s.subnets()) for ipnetwork_k8s in ipnetworks_k8s
                    )
                    yield from ipnetworks_k8s_subnets

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}(\n"
            f"\t{self.configinfra=}\n"
            f"\t{self.configkubernetes=}\n"
            f"\t{self.configtaloslinux=}\n"
            f"\t{self.path_file_kubeconfig=}\n"
            f"\t{self.path_file_taloslinux_config_controlplane=}\n"
            f"\t{self.path_file_taloslinux_config_firewall=}\n"
            f"\t{self.path_file_taloslinux_config_talosconfig=}\n"
            f"\t{self.path_file_taloslinux_config_worker=}\n"
            f"\t{self.url_endpoint_k8sapiserver=})\n"
        )

    def __init__(
        self,
        *,
        configinfra: ConfigInfra,
        configkubernetes: ConfigKubernetes,
        configtaloslinux: ConfigTaloslinux,
        path_dir_configuration: Path,
    ) -> None:
        # TODO: Exclude all routable subnets on hypervisor host.
        # TODO: Exclude loadbalancers.
        # TODO: Check if `self.dnsresolvers` items are compatible with IP address families of `ipam_k8s`.
        # TODO: Add reverse DNS names.
        self.configinfra = configinfra
        self.configkubernetes = configkubernetes
        self.configtaloslinux = configtaloslinux
        self.path_dir_configuration = path_dir_configuration
        self.path_file_taloslinux_config_controlplane = self.path_dir_configuration / "controlplane.yml"
        self.path_file_taloslinux_config_firewall = self.path_dir_configuration / "firewall.yml"
        self.path_file_taloslinux_config_talosconfig = self.path_dir_configuration / "config.yml"
        self.path_file_taloslinux_config_worker = self.path_dir_configuration / "worker.yml"
        self.path_file_taloslinux_secrets = self.path_dir_configuration / "secrets.yml"
        self.path_file_kubeconfig = self.path_dir_configuration / "kubeconfig.yml"
        self.configkubernetes.ipnetworks_k8s_pods, self.configkubernetes.ipnetworks_k8s_services = zip(
            *self._produce_subnets(),
            strict=True,
        )
        match self.configtaloslinux.loadbalancing_nodes:
            case None:
                # Convention: first control plane nodes first network interface's IP address is the endpoint address.
                self.ipaddress_endpoint_k8sapiserver = self.configinfra.networkinterfaces_nodes_controlplane[0][
                    0
                ].ipinterface.ip
            case LoadbalancingNodesL7(ipinterface_loadbalancer_nodes=ipinterface_loadbalancer_nodes):
                self.ipaddress_endpoint_k8sapiserver = ipinterface_loadbalancer_nodes.ip
        # TODO: Use URL type.
        self.url_endpoint_k8sapiserver: str = (
            f"https://{f"[{self.ipaddress_endpoint_k8sapiserver!s}]"
                                                    if isinstance(self.ipaddress_endpoint_k8sapiserver, IPv6Address)
                                                    else str(self.ipaddress_endpoint_k8sapiserver)}:6443"
        )

    @abstractmethod
    async def deploy(self) -> None:
        """Deploy as a new instance."""

    @abstractmethod
    async def destroy(self) -> None:
        """Destroy the deployment on the hypervisor node. ⚠️ Destructive."""

    @abstractmethod
    async def connect(self) -> None:
        """Connect to the deployment instance."""

    async def configure(self) -> None:
        if self.configtaloslinux.generate_taloslinux_configuration:
            await self._generate_configuration()
            await self._patch_configuration()
        else:
            await self._validate_configuration()

    async def _generate_configuration(self) -> None:
        command_talosctl_gen_config = [
            "talosctl",
            "gen",
            "config",
            self.configkubernetes.name_cluster,
            self.url_endpoint_k8sapiserver,
            # TODO: Use Ceph/Rook
            # "--config-patch @openebs_patch.yml "
            "--force",
            f"--kubernetes-version={self.configkubernetes.version}",
            f"--output-dir={self.path_dir_configuration!s}",
            f"--talos-version=v{self.configtaloslinux.version}",
            "--with-docs=false",
            "--with-examples=false",
            f"--with-cluster-discovery={str(self.configtaloslinux.enable_discovery).lower()}",
            f"--with-kubespan={str(self.configtaloslinux.enable_kubespan).lower()}",
            f"--with-secrets={self.path_file_taloslinux_secrets!s}",
        ]
        if not await self.path_file_taloslinux_secrets.exists():
            command_talosctl_gen_secrets = [
                "talosctl",
                "gen",
                "secrets",
                "--force",
                f"--output-file={self.path_file_taloslinux_secrets!s}",
            ]
            await execute(*command_talosctl_gen_secrets, stderr=logger, stdout=logger, text=True)
        await execute(*command_talosctl_gen_config, stderr=logger, stdout=logger, text=True)
        # TODO: Load as package resource instead of by path
        copy(Path("firewall.yml"), self.path_file_taloslinux_config_firewall.with_suffix(".orig.yml"))
        # Adds extension to file, uses standard name for configuration lookup https://www.talos.dev/v1.7/learn-more/talosctl/#client-configuration.
        move(self.path_dir_configuration / "talosconfig", self.path_file_taloslinux_config_talosconfig)
        move(
            # `talosctl` always uses `.yaml`, while we standardize on `.yml`.
            self.path_file_taloslinux_config_controlplane.with_suffix(".yaml"),
            self.path_file_taloslinux_config_controlplane.with_suffix(
                ".orig.yml",
            ),
        )
        move(
            self.path_file_taloslinux_config_worker.with_suffix(".yaml"),
            self.path_file_taloslinux_config_worker.with_suffix(".orig.yml"),
        )

    async def _patch_configuration(self) -> None:
        await self._patch_configuration_universal()
        await self._validate_configuration()
        await self._patch_configuration_pernode()
        command_talosctl_config_endpoint = [
            "talosctl",
            f"--talosconfig={self.path_file_taloslinux_config_talosconfig!s}",
            "config",
            "endpoint",
            str(self.configinfra.ipaddress_endpoint_k8sapiserver_workstation),
        ]
        await execute(*command_talosctl_config_endpoint, stderr=logger, stdout=logger, text=True)

    def produce_path_configuration_node(self, *, index_node: int, is_controlplane: bool) -> Path:
        return (
            (
                self.path_file_taloslinux_config_controlplane
                if is_controlplane
                else self.path_file_taloslinux_config_worker
            )
            .with_suffix("")
            .with_suffix(
                f".{index_node}.yml",
            )
        )

    async def _patch_configuration_pernode(self) -> None:
        for index_node, networkinterfaces_node in enumerate(self.configinfra.networkinterfaces_nodes):
            is_controlplane, _ = self.configinfra.describe_node(index=index_node)
            async with (
                await (
                    self.path_file_taloslinux_config_controlplane
                    if is_controlplane
                    else self.path_file_taloslinux_config_worker
                ).open(mode="r", encoding="utf-8") as file_input,
                await self.produce_path_configuration_node(index_node=index_node, is_controlplane=is_controlplane).open(
                    mode="w",
                    encoding="utf-8",
                ) as file_output,
            ):
                with YAML(output=file_output.wrapped, pure=True, typ="safe") as yaml:
                    yaml.default_flow_style = False
                    yaml.explicit_start = True
                    yaml.preserve_quotes = True
                    for document in yaml.load_all(file_input.wrapped):
                        document["machine"]["network"]["interfaces"] = [
                            {
                                "interface": self.configinfra.name_networkinterface_parent,
                                # TODO: Only works with L2 connectivity between nodes.
                                "vip": None,
                                "vlans": [
                                    {
                                        # TODO: Create config for each node.
                                        "addresses": [
                                            str(networkinterface.ipinterface.ip)
                                            for networkinterface in networkinterfaces_node
                                        ],
                                        "routes": [
                                            # Convention: the first and second are the primary IPv4 and IPv6 interfaces.
                                            {
                                                "gateway": str(networkinterfaces_node[0].gateway),
                                                "network": str(IPv4Interface("0.0.0.0/0")),
                                            },
                                            {
                                                "gateway": str(networkinterfaces_node[1].gateway),
                                                "network": str(IPv6Interface("::/0")),
                                            },
                                        ],
                                        "dhcp": False,
                                        "vlanId": self.configinfra.vlanid,
                                    },
                                ],
                            },
                        ]
                        yaml.dump(document)
                await file_output.flush()

    async def _patch_configuration_universal(self) -> None:
        paths_file_taloslinux_config = [
            self.path_file_taloslinux_config_firewall.with_suffix(".orig.yml"),
            self.path_file_taloslinux_config_controlplane.with_suffix(".orig.yml"),
            self.path_file_taloslinux_config_worker.with_suffix(".orig.yml"),
        ]
        # TODO: Clean up file path handling.
        for index, path_file_taloslinux_config in enumerate(paths_file_taloslinux_config):
            async with (
                await path_file_taloslinux_config.open(mode="r") as file_input,
                await path_file_taloslinux_config.with_suffix("").with_suffix(".yml").open(mode="w") as file_output,
            ):
                with YAML(output=file_output.wrapped, pure=True, typ="safe") as yaml:
                    yaml.default_flow_style = False
                    yaml.explicit_start = True
                    yaml.preserve_quotes = True
                    if not index:
                        self._patch_configuration_universal_firewall(file_input=file_input, yaml=yaml)
                    else:
                        document = yaml.load(file_input.wrapped)
                        if index == 1:
                            document = self._patch_configuration_universal_controlplane(document=document)
                        # https://github.com/kubernetes/kubernetes/issues/115300
                        document["cluster"]["network"]["dnsDomain"] = f"{self.configkubernetes.name_cluster}.clusters."
                        document["cluster"]["network"]["podSubnets"] = [
                            str(ipnetwork_k8s_pods) for ipnetwork_k8s_pods in self.configkubernetes.ipnetworks_k8s_pods
                        ]
                        document["cluster"]["network"]["serviceSubnets"] = [
                            str(ipnetwork_k8s_services)
                            for ipnetwork_k8s_services in self.configkubernetes.ipnetworks_k8s_services
                        ]
                        document["machine"]["certSANs"].extend(
                            (
                                *(
                                    str(networkinterface_node.ipinterface.ip)
                                    for networkinterfaces_node in self.configinfra.networkinterfaces_nodes
                                    for networkinterface_node in networkinterfaces_node
                                ),
                                *(
                                    [str(self.configtaloslinux.loadbalancing_nodes.ipinterface_loadbalancer_nodes)]
                                    if self.configtaloslinux.loadbalancing_nodes is not None
                                    else []
                                ),
                                # TODO: Only exclude actual `self.endpoint_k8sapiserver`.
                                # Add loopback IP addresses to allow for port forwarding.
                                str(IPv6Interface("::1/128").ip),
                                # The whole subnet isn't being added here, it's unusual .
                                str(IPv4Interface("127.0.0.1/8").ip),
                            ),
                        )
                        document["machine"]["certSANs"] = sorted(document["machine"]["certSANs"])
                        document["machine"]["features"]["kubePrism"]["enabled"] = False
                        document["machine"]["features"]["hostDNS"] = {
                            "enabled": True,
                            "forwardKubeDNSToHost": True,
                            "resolveMemberNames": True,
                        }
                        if "extraArgs" not in document["machine"]["kubelet"]:
                            document["machine"]["kubelet"]["extraArgs"] = {}
                        document["machine"]["kubelet"]["extraArgs"]["anonymous-auth"] = False
                        if self.configkubernetes.ipaddress_bind_controlplane_k8s is not None:
                            if "extraConfig" not in document["machine"]["kubelet"]:
                                document["machine"]["kubelet"]["extraConfig"] = {}
                            document["machine"]["kubelet"]["extraConfig"][
                                "address"
                            ] = self.configkubernetes.ipaddress_bind_controlplane_k8s
                        document["machine"]["network"]["nameservers"] = [
                            str(dnsresolver) for dnsresolver in self.configtaloslinux.dnsresolvers
                        ]
                        if "kernel" not in document["machine"]:
                            document["machine"]["kernel"] = {}
                        if "modules" not in document["machine"]["kernel"]:
                            document["machine"]["kernel"]["modules"] = []
                        modules = (
                            {"name": module} for module in ("nvidia", "nvidia_uvm", "nvidia_drm", "nvidia_modeset")
                        )
                        document["machine"]["kernel"]["modules"].extend(modules)
                        yaml.dump(document)
                await file_output.flush()

    def _patch_configuration_universal_firewall(self, file_input: AsyncFile[str], yaml: YAML) -> None:
        for document in yaml.load_all(file_input.wrapped):
            match document.get("kind"), document.get("name"):
                case ("NetworkRuleConfig", "kubelet-ingress" | "trustd-ingress" | "cni-vxlan"):
                    document["ingress"] = [
                        {"subnet": str(ipnetwork_k8s_pods)}
                        for ipnetwork_k8s_pods in self.configkubernetes.ipnetworks_k8s_pods
                    ]
                case ("NetworkRuleConfig", "etcd-ingress"):
                    document["ingress"] = [
                        {
                            "subnet": f"{networkinterface_node_controlplane.ipinterface.ip}/"
                            f"{networkinterface_node_controlplane.ipinterface.ip.max_prefixlen}",
                        }
                        for networkinterfaces_node_controlplane in self.configinfra.networkinterfaces_nodes_controlplane
                        for networkinterface_node_controlplane in networkinterfaces_node_controlplane
                    ]
            yaml.dump(document)

    def _patch_configuration_universal_controlplane(self, document: dict[str, Any]) -> dict[str, Any]:
        for component_k8s in ("proxy", "apiServer", "scheduler", "controllerManager"):
            # TODO: `--advertise-address``
            document["cluster"][component_k8s]["extraArgs"] = {}
            if self.configkubernetes.ipaddress_bind_controlplane_k8s is not None:
                document["cluster"][component_k8s]["extraArgs"][
                    "bind-address"
                ] = self.configkubernetes.ipaddress_bind_controlplane_k8s
            if component_k8s == "apiServer":
                document["cluster"][component_k8s]["certSANs"].extend(
                    (
                        *(
                            str(networkinterface_node.ipinterface.ip)
                            for networkinterfaces_node in self.configinfra.networkinterfaces_nodes_controlplane
                            for networkinterface_node in networkinterfaces_node
                        ),
                        # TODO: Not needed, added by `talosctl`?
                        *(
                            [
                                str(
                                    self.configtaloslinux.loadbalancing_nodes.ipinterface_loadbalancer_nodes,
                                ),
                            ]
                            if self.configtaloslinux.loadbalancing_nodes is not None
                            else []
                        ),
                        *(
                            # Add loopback IP addresses to allow for port forwarding.
                            str(IPv6Interface("::1/128").ip),
                            # The whole subnet isn't being added here, it's unusual .
                            str(IPv4Interface("127.0.0.1/8").ip),
                        ),
                    ),
                )
                document["cluster"][component_k8s]["certSANs"] = sorted(
                    document["cluster"][component_k8s]["certSANs"],
                )
            elif component_k8s == "proxy":
                document["cluster"][component_k8s]["mode"] = "nftables"
                document["cluster"][component_k8s]["extraArgs"]["bind-address-hard-fail"] = True
                document["cluster"][component_k8s]["extraArgs"]["feature-gates"] = "NFTablesProxyMode=true"
            elif component_k8s in ("scheduler", "controllerManager"):
                document["cluster"][component_k8s]["extraArgs"]["authentication-tolerate-lookup-failure"] = False
            # TODO: `document["cluster"]["etcd"]["advertisedSubnets"] = str(ipnetwork_nodes_local)`
        return document

    async def _validate_configuration(self) -> None:
        for path_file_config in (
            self.path_file_taloslinux_config_controlplane,
            self.path_file_taloslinux_config_worker,
        ):
            command_talosctl_talosconfig_validate = [
                "talosctl",
                f"--talosconfig={self.path_file_taloslinux_config_talosconfig!s}",
                "validate",
                "--config",
                str(path_file_config),
                "--mode",
                "cloud",
                "--strict",
            ]
            await execute(*command_talosctl_talosconfig_validate, stderr=logger, stdout=logger, text=True)


def configure_ipam_nodes(configuration: dict[str, Any]) -> IpamNodes:
    ipnetworks_nodes: list[IPv4Network | IPv6Network]
    ipam_nodes_str = str(configuration["ipam_nodes"])
    exclusions: list[IPv4Address | IPv6Address] = [
        *[IPv4Address(str(exclusion)) for exclusion in configuration["exclusions_ipv4"]],
        *[IPv6Address(str(exclusion)) for exclusion in configuration["exclusions_ipv6"]],
    ]
    match ipam_nodes_str:
        case "nonsegmented":
            ipnetworks_nodes = [
                *[IPv4Network(ipnetworks_nodes) for ipnetworks_nodes in configuration["ipnetworks_nodes_ipv4"]],
                *[IPv6Network(ipnetworks_nodes) for ipnetworks_nodes in configuration["ipnetworks_nodes_ipv6"]],
            ]
            return IpamNodesNonsegmented(exclusions=exclusions, ipnetworks_nodes=ipnetworks_nodes)
        case "segmented":
            ipnetworks_nodes_controlplane: list[IPv4Network | IPv6Network] = [
                *[IPv4Network(dnsresolver) for dnsresolver in configuration["ipnetworks_nodes_controlplane_ipv4"]],
                *[IPv6Network(dnsresolver) for dnsresolver in configuration["ipnetworks_nodes_controlplane_ipv6"]],
            ]
            ipnetworks_nodes_worker: list[IPv4Network | IPv6Network] = [
                *[IPv4Network(dnsresolver) for dnsresolver in configuration["ipnetworks_nodes_worker_ipv4"]],
                *[IPv6Network(dnsresolver) for dnsresolver in configuration["ipnetworks_nodes_worker_ipv6"]],
            ]
            return IpamNodesSegmented(
                exclusions=exclusions,
                ipnetworks_nodes_controlplane=ipnetworks_nodes_controlplane,
                ipnetworks_nodes_worker=ipnetworks_nodes_worker,
            )
        case _:
            msg = f"`ipam_nodes` `{ipam_nodes_str}` unknown"
            raise NotImplementedError(msg)


def configure_loadbalancing_nodes(configuration: dict[str, Any]) -> LoadbalancingNodes | None:
    ipinterface_loadbalancer_nodes: IPv4Interface | IPv6Interface
    if ipinterface_loadbalancer_nodes_str := configuration["ipinterface_loadbalancer_nodes"]:
        try:
            ipinterface_loadbalancer_nodes = IPv6Interface(ipinterface_loadbalancer_nodes_str)
        except AddressValueError:
            # pylint: disable-next=redefined-variable-type
            ipinterface_loadbalancer_nodes = IPv4Interface(ipinterface_loadbalancer_nodes_str)
    match str(configuration["loadbalancing"]):
        case "L4":
            return LoadbalancingNodesL4(
                ipinterface_loadbalancer_nodes=ipinterface_loadbalancer_nodes,
            )
        case "L7":
            return LoadbalancingNodesL7(
                ipinterface_loadbalancer_nodes=ipinterface_loadbalancer_nodes,
            )
        case "none":
            return None
        case _:
            msg = f"`loadbalancing` `{configuration["loadbalancing"]!s}` unknown"
            raise NotImplementedError(msg)


def configure_ipam_k8s(configuration: dict[str, Any]) -> IpamK8s:
    ipnetworks_k8s_pods: list[IPv4Network | IPv6Network]
    ipnetworks_k8s_services: list[IPv4Network | IPv6Network]
    ipam_k8s: IpamK8s
    ipam_k8s_str = str(configuration["ipam_k8s"])
    if ipam_k8s_str == "nonsegmented":
        ipnetworks_k8s: list[IPv4Network | IPv6Network] = [
            *[IPv4Network(ipnetworks_k8s) for ipnetworks_k8s in IPv4Network(configuration["ipnetworks_k8s_ipv4"])],
            *[IPv6Network(ipnetworks_k8s) for ipnetworks_k8s in IPv4Network(configuration["ipnetworks_k8s_ipv6"])],
        ]
        ipam_k8s = IpamK8sNonsegmented(ipnetworks_k8s=ipnetworks_k8s)
    elif ipam_k8s_str == "segmented":
        ipnetworks_k8s_pods = [
            *[IPv4Network(dnsresolver) for dnsresolver in configuration["ipnetworks_k8s_pods_ipv4"]],
            *[IPv6Network(dnsresolver) for dnsresolver in configuration["ipnetworks_k8s_pods_ipv6"]],
        ]
        ipnetworks_k8s_services = [
            *[IPv4Network(dnsresolver) for dnsresolver in configuration["ipnetworks_k8s_services_ipv4"]],
            *[IPv6Network(dnsresolver) for dnsresolver in configuration["ipnetworks_k8s_services_ipv6"]],
        ]
        ipam_k8s = IpamK8sSegmented(
            ipnetworks_k8s_pods=ipnetworks_k8s_pods,
            ipnetworks_k8s_services=ipnetworks_k8s_services,
        )
    else:
        msg = f"`ipam_k8s` `{ipam_k8s_str}` unknown"
        raise NotImplementedError(msg)
    return ipam_k8s
