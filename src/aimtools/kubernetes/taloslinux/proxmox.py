from __future__ import annotations

from shutil import copy
from tempfile import TemporaryDirectory
from typing_extensions import deprecated

from anyio import Path

from aimtools.cli import execute
from aimtools.kubernetes.taloslinux import Taloslinux


class Proxmox(Taloslinux):
    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(\n\t{super().__repr__()})\n"

    @deprecated("Configure networking along with Talos Linux configuration rather than cloud-init.")
    async def generate_cloudinit_isos(self, index: int) -> Path:
        """Generate ISO image files for cloud-init

        Used as a generic method to configure VMs, e.g. IP addressing."""
        with TemporaryDirectory() as path_dir_temp:
            path_dir_iso = Path(path_dir_temp) / "iso"
            await path_dir_iso.mkdir()
            for path_file_config in (
                self.path_file_taloslinux_config_controlplane,
                self.path_file_taloslinux_config_worker,
            ):
                copy(path_file_config, path_dir_iso / "user-data")
                async with await (path_dir_iso / "meta-data").open("wt") as file_metadata:
                    await file_metadata.write(f"local-hostname: '{self.configinfra.describe_node(index)}'")
                async with await (path_dir_iso / "network-config").open("wt") as file_networkconfig:
                    # TODO: Use YAML library.
                    # TODO: Also set gateway.
                    await file_networkconfig.write(
                        f"""---
                            version: 1
                            config:
                            - type: physical
                                name: eth0
                                mac_address: "52:54:00:12:34:00"
                                subnets:
                                    - type: static
                                    address: {self.configinfra.networkinterfaces_nodes[index]}
                            """,
                    )
                await execute(
                    "genisoimage",
                    "-output",
                    "cidata.iso",
                    "-V",
                    "cidata",
                    "-r",
                    "-J",
                    "user-data",
                    "meta-data",
                    "network-config",
                    cwd=path_dir_iso,
                    text=True,
                )
            return path_dir_iso

    async def connect(self) -> None:
        raise NotImplementedError()

    async def deploy(self) -> None:
        _name_file_image = "nocloud-amd64.raw.xz"
        # TODO: Move to Deployment.
        path_file_diskimage_taloslinux = Path(_name_file_image)
        if not await Path(path_file_diskimage_taloslinux.stem).exists():
            if not await path_file_diskimage_taloslinux.exists():
                # TODO: Extract in tempdir.
                await execute(
                    "ssh",
                    "-p",
                    str(self.configinfra.opensshclient_port),
                    self.configinfra.opensshclient_host,
                    "--",
                    "curl",
                    "--fail",
                    "--location",
                    "--remote-name",
                    "--",
                    f"https://github.com/siderolabs/talos/releases/download/v{self.configtaloslinux.version}/{_name_file_image}",
                    text=True,
                )
            await execute(
                "ssh",
                "-p",
                str(self.configinfra.opensshclient_port),
                self.configinfra.opensshclient_host,
                "--",
                "unxz",
                str(path_file_diskimage_taloslinux),
                text=True,
            )
        path_dir_proxmoxsnippets = Path("/var/lib/vz/snippets/")
        # TODO: mkdir /var/lib/vz/snippets
        path_symlink_taloslinux_config_controlplane = (
            path_dir_proxmoxsnippets / self.path_file_taloslinux_config_controlplane.name
        )
        if not await path_symlink_taloslinux_config_controlplane.is_symlink():
            await path_symlink_taloslinux_config_controlplane.symlink_to(
                await self.path_file_taloslinux_config_controlplane.resolve(),
            )
        path_symlink_taloslinux_config_worker = path_dir_proxmoxsnippets / self.path_file_taloslinux_config_worker.name
        if not await path_symlink_taloslinux_config_worker.is_symlink():
            await path_symlink_taloslinux_config_worker.symlink_to(
                await self.path_file_taloslinux_config_worker.resolve(),
            )
        for index in range(self.configinfra.n_nodes_controlplane + self.configinfra.n_nodes_worker):
            is_controlplane, name_node = self.configinfra.describe_node(index=index)
            path_dir_iso = await self.generate_cloudinit_isos(index=index)
            await execute(
                "ssh",
                "-p",
                str(self.configinfra.opensshclient_port),
                self.configinfra.opensshclient_host,
                "--",
                "qm",
                "create",
                name_node,
                "--memory",
                "4096" if is_controlplane else "32768",
                "--net0",
                "virtio,bridge=vmbr0",
                "--scsihw",
                "virtio-scsi-pci",
                "--boot",
                "order=scsi0",
                "--cores",
                "4" if is_controlplane else "8",
                "--cpu",
                "host",
                "--name",
                name_node,
                "--net0",
                "e1000,bridge=vmbr0",
                "--numa",
                "0",
                "--ostype",
                "l26",
                "--scsihw",
                "virtio-scsi-single",
                "--scsi0",
                f"local-lvm:0,import-from={(path_dir_iso / 'cidata.iso')!s}",
                "--serial0",
                "socket",
                "--sockets",
                "1",
                "--tags",
                "taloslinux",
                text=True,
            )
            # TODO: Is this needed?
            await execute(
                "ssh",
                "-p",
                str(self.configinfra.opensshclient_port),
                self.configinfra.opensshclient_host,
                "--",
                "qm",
                "set",
                name_node,
                "--ide2",
                "local-lvm:cloudinit",
                text=True,
            )
            await execute(
                "ssh",
                "-p",
                str(self.configinfra.opensshclient_port),
                self.configinfra.opensshclient_host,
                "--",
                "qm",
                "importdisk",
                name_node,
                path_file_diskimage_taloslinux.stem,
                "local-lvm",
                "--format=raw",
                text=True,
            )
            await execute(
                "ssh",
                "-p",
                str(self.configinfra.opensshclient_port),
                self.configinfra.opensshclient_host,
                "--",
                "qm",
                "resize",
                name_node,
                "scsi0",
                "50G",
                text=True,
            )
            await execute(
                "ssh",
                "-p",
                str(self.configinfra.opensshclient_port),
                self.configinfra.opensshclient_host,
                "--",
                "qm",
                "set",
                name_node,
                "--cicustom",
                f"user=local:snippets/{(path_symlink_taloslinux_config_controlplane.name if is_controlplane
                                            else path_symlink_taloslinux_config_worker.name)!s}",
                text=True,
            )
            await execute(
                "ssh",
                "-p",
                str(self.configinfra.opensshclient_port),
                self.configinfra.opensshclient_host,
                "--",
                "qm",
                "cloudinit",
                "update",
                name_node,
                text=True,
            )

    async def destroy(self) -> None:
        raise NotImplementedError()
