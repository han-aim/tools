# Datalab IT platform: Deployment tool<!-- omit in toc -->

- [Introduction](#introduction)
  - [Vision](#vision)
  - [Comparison to other solutions](#comparison-to-other-solutions)
    - [References](#references)
  - [Non-fundamental limitations](#non-fundamental-limitations)
- [To use](#to-use)
  - [Install on your workstation](#install-on-your-workstation)
  - [Prepare the server](#prepare-the-server)
    - [Alternative A: Alpine Linux 3.20 (recommended, unless using NVIDIA CUDA)](#alternative-a-alpine-linux-320-recommended-unless-using-nvidia-cuda)
    - [Alternative B: Ubuntu 24.04 (not recommended, unless using NVIDIA CUDA)](#alternative-b-ubuntu-2404-not-recommended-unless-using-nvidia-cuda)
  - [Create a deployment configuration on your workstation](#create-a-deployment-configuration-on-your-workstation)
  - [Configure OpenSSH to connect to the hypervisor host](#configure-openssh-to-connect-to-the-hypervisor-host)
  - [Prepare and use your workstation](#prepare-and-use-your-workstation)
  - [To deploy on Podman instance on your macOS or Windows workstation](#to-deploy-on-podman-instance-on-your-macos-or-windows-workstation)
  - [To use the Storage Attached Network (SAN) on R29](#to-use-the-storage-attached-network-san-on-r29)
  - [To use NVIDIA CUDA inside Pods](#to-use-nvidia-cuda-inside-pods)
  - [To (re)connect to the control plane (`connect`)](#to-reconnect-to-the-control-plane-connect)
  - [To clean up before a new deployment (`destroy`)](#to-clean-up-before-a-new-deployment-destroy)

## Introduction

As part of the Datalab IT-platform, this is a tool to deploy a modern IT-infrastructure for data-intensive software systems, such as data pipelines, AI-based applications, etc.

The IT infrastructure is based on Kubernetes on top of Talos Linux on top of Podman.

Using the Deployment tool, you can host your own Kubernetes instance on your workstation, self-managed server or cloud machine.

### Vision

In the short term, this tool is used to deploy small, single server IT-infra, consisting of three control plane nodes and a few worker nodes.
Because this tool operates quickly and solves the more complex technical design, it leaves up to users only the most basic configuration parameters.
This makes the tool suitable for students and student groups with substantial IT knowledge to deploy their own IT-infra and use this IT-platform, with little risks.
The intended user groups are e.g., HAN AIM IT-savvy researchers and educators, students of the HAN AIM MSc Applied Data Science program, or HAN AIM HBO-ICT students.

In the longer run, [as external constraints disappear](#non-fundamental-limitations), this tool would be one of many used by IT ops professionals running centralized services (at least one ‘central’ Datalab IT-platform) for said intended user groups, as the tool would support a different, more scalable system architecture.

### Comparison to other solutions

| Software or service                                                                                                                                | Vendor lock-in | Aimed at servers | Security | Bare metal support | Manageability | Configurability |
| -------------------------------------------------------------------------------------------------------------------------------------------------- | -------------- | ---------------- | -------- | ------------------ | ------------- | --------------- |
| [K3s](https://k3s.io/)                                                                                                                             | ➕/➖          | ➕               | ➖       | ➖                 | ➖            | ➖              |
| [Kind](https://kind.sigs.k8s.io/)                                                                                                                  | ➕             | ➖               | ➖       | ➖                 | ➖            | ➖              |
| Kubernetes-as-a-Service ([Azure AKS](https://azure.microsoft.com/en-us/products/kubernetes-service), [AWS EKS](https://aws.amazon.com/eks/), etc.) | ➖             | ➕               | ➖       | ➖                 | ➖            | ➖              |
| [Kubeadm](https://kubernetes.io/docs/reference/setup-tools/kubeadm/)/[MicroK8s](https://microk8s.io/)/[Rancher](https://www.rancher.com/)          | ➕             | ➕               | ➖       | ➖                 | ➖            | ➖              |
| [Minikube](https://minikube.sigs.k8s.io/docs/)                                                                                                     | ➕             | ➖               | ➖       | ➖                 | ➖            | ➖              |
| [Rancher Desktop](https://rancherdesktop.io/)                                                                                                      | ➕             | ➖               | ➖       | ➖                 | ➖            | ➖              |
| [Talos Linux](https://www.talos.dev/)                                                                                                              | ➕             | ➕               | ➕       | ➕                 | ➕            | ➕              |
| VMs or plain containers                                                                                                                            | ➕             | ➕/➖            | ➖       | ➕/➖              | ➖            | ➖              |

#### References

Due to severe time constraints and lack of (interest by) potential readers, there's no motivation for the solution evaluation here, nor does it cover all popular alternatives.
Please read some sources to gain an additional understanding.
In case you question an evaluation score, please contact Sander Maijers.
The information will be elaborated as needed, on demand.

1. [Small Kubernetes for your local experiments: k0s, MicroK8s, kind, k3s, and Minikube](https://blog.palark.com/small-local-kubernetes-comparison/)
1. [MicroK8s vs K3s vs minikube](https://microk8s.io/compare)
1. [Principles | Minikube](https://minikube.sigs.k8s.io/docs/contrib/principles/)
1. [4 Reasons to Use Talos Linux In AWS](https://www.siderolabs.com/blog/4-reasons-to-use-omni-over-eks/)
1. [K3s vs. Talos Linux: What’s the difference](https://www.civo.com/blog/k3s-vs-talos-linux)

### Non-fundamental limitations

Due to limitations of the hardware and IaaS platforms this tool is designed to work with, this tool deploys Talos Linux nodes as Podman containers, sometimes on top of cloud provider VMs, rather than as bare metal dedicated nodes.
This increases deployment complexity, and in turn reduces the user experience of the platform as well as the security, bare metal support and manageability benefits of Talos Linux.

As SURF Research Cloud (SRC) evolves to support native Talos Linux, and on-premises operations happens in a context of more hardware nodes available, the tool will be simplified and these limitations will disappear.

<!-- TODO: firewall.yaml -->

## To use

### Install on your workstation

1. Set up your workstation with [AIM Tools](../../../../README.md).
1. Install [`kubectl`](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
1. Install [`talosctl`](https://www.talos.dev/v1.7/talos-guides/install/talosctl/).

### Prepare the server

You can deploy on a cloud, e.g., in an SRC Workspace or self-hosted, e.g., iDRAC on a HAN on-premises (R26/R29) bare-metal server.

**NOTE**: these steps become easier and briefer if you were to use an up-to-date, brief Linux distro such as Alpine Linux.
Alas, not all distros are available in SRC.

<!-- TODO: https://stackoverflow.com/a/50341882/1175508 -->

#### Alternative A: Alpine Linux 3.20 (recommended, unless using NVIDIA CUDA)

These steps start from the Alpine Linux installer.

1. Generate a keypair for OpenSSH on your workstation.

1. Log in as `root`.

1. Configure the OS, for example with [this `answerfile`](answerfile):

   ```sh
   setup-alpine \
     -f answerfile
   ```

1. Reboot.

1. Log in as `root`.

The next few steps start from a preinstalled OS.

1. Install Podman, `rsync`, `socat`, `util-linux`.

   ```sh
   apk \
     add \
     podman \
     rsync \
     socat \
     util-linux
   ```

1. Configure Podman [to be able to run as `root`](https://wiki.alpinelinux.org/wiki/Podman#Running_as_root).

#### Alternative B: Ubuntu 24.04 (not recommended, unless using NVIDIA CUDA)

These steps assume a preinstalled OS and being logged in as superuser.
[Upgrade Ubuntu 22.04 to 24.04](https://www.tecmint.com/upgrade-ubuntu-22-04-to-24-04/).

1. Install Podman ≥ 4.9 and [`socat` ≥ 1.8](http://www.dest-unreach.org/socat/).

   ```sh
   apt-get \
     install \
     podman \
     socat
   ```

1. Update Netavark to ≥ 1.5.

   ```sh
   apt-get \
     install \
     cargo \
     protobuf-compiler
   cargo \
     install \
     netavark
   ln \
     -fs \
     /root/.cargo/bin/netavark \
     /usr/lib/podman/netavark
   ```

### Create a deployment configuration on your workstation

1. Create a configuration directory named after the specific deployment instance, e.g., `~/Documents/$AIMTOOLS_OPENSSHCLIENT_HOST/`, where
   `$AIMTOOLS_OPENSSHCLIENT_HOST` is henceforth an environment variable containing a deployment name of your choosing.
   Change the working directory to that directory.

   ```sh
   cd ~/"Documents/${AIMTOOLS_OPENSSHCLIENT_HOST:?}/"
   ```

1. Create a configuration file named `configuration.toml` in the configuration directory.
   [Sample configurations](configurations/) are in this repository for workstation-based Podman, SRC and R29.
   Environment variables can be kept easily using `.env` (dotenv) files.
   Create a [`.env`](https://github.com/theskumar/python-dotenv/blob/main/README.md) (dotenv) file there based on, e.g., a sample from [`configurations/*/.env.sample`](configurations/taloslinux-podman/.env.sample).
   The `.env.sample` files have sensible example values for `$AIMTOOLS_OPENSSHCLIENT_HOST`, `$AIMTOOLS_OPENSSHCLIENT_PORT`, `$AIMTOOLS_PATH_FILE_OPENSSHCLIENT_KNOWNHOSTS`, `$AIMTOOLS_PATH_FILE_OPENSSHCLIENT_PRIVATEKEY`.

<!-- TODO: Automate this with AIM Tools. -->

### Configure OpenSSH to connect to the hypervisor host

Configure OpenSSH so that the hypervisor host accepts key-based authentication for the `root` user, and keep the private key available to systems that should be able to communicate with the IT-infra that the Deployment tool deploys, such as your workstation.

1. Generate an OpenSSH keypair.
   On your workstation:

   ```sh
   ssh-keygen \
     -f "$HOME/.ssh/$name_keypair" \
     -t 'ed25519'
   ```

   You may name (`$name_keypair`) `keypair_taloslinux-server-root` or whatever is most distinctive and clear.
   Also record the full file path in the `$AIMTOOLS_PATH_FILE_OPENSSHCLIENT_PRIVATEKEY` environment variable.

1. Configure the OpenSSH connection.
   Add the following to the `~/.ssh/config` file on your workstation (but consider the next paragraph):

   ```ssh-config
   Host $AIMTOOLS_OPENSSHCLIENT_HOST
     HostName $hostname_endpoint_hypervisor
     IdentityFile ${AIMTOOLS_PATH_FILE_OPENSSHCLIENT_PRIVATEKEY}
     Port $AIMTOOLS_OPENSSHCLIENT_HOST
     User root
     UserKnownHostsFile ${AIMTOOLS_PATH_FILE_OPENSSHCLIENT_KNOWNHOSTS}
   ```

   Replace `$hostname_endpoint_hypervisor`, `$AIMTOOLS_OPENSSHCLIENT_HOST` and `$AIMTOOLS_OPENSSHCLIENT_PORT` with literal text.
   The other environment variables are preferably not replaced.
   These environment variables must then be set each time you use this tool, and other variables used previously, such as `$hostname_endpoint_hypervisor` must also match with the Deployment tool’s configuration key with the same name.

1. On the hypervisor host, configure OpenSSH server to accept connections with this keypair:

   ```console
   $ sudo -s
   # umask 077 ;
   # test -d /root/.ssh || mkdir /root/.ssh ;
   # cat >> /root/.ssh/authorized_keys
   ```

   After issuing the last command, paste the public key, press Enter and press Ctrl-D.

1. Reconfigure the OpenSSH server’s security, since sometimes it’s too strict in some ways and too loose in others:

   ```sh
   printf \
     '%s %s\n' \
     'PasswordAuthentication' 'no' \
     'AllowStreamLocalForwarding' 'yes' \
     'AllowTcpForwarding' 'yes' \
     >/etc/ssh/sshd_config.d/taloslinux.conf
   ```

### Prepare and use your workstation

1. Run the deployment tool:

   ```sh
   python3 \
     -m aimtools \
     kubernetes \
     taloslinux \
     --generate_taloslinux_configuration \
     . \
     deploy
   ```

   ⚠️ Adjust `--generate_taloslinux_configuration` in subsequent invocations to prevent overwriting your Talos Linux
   configuration files and secrets! After a while the deployment process pauses, and you’re asked to press any key to
   continue after setting up a connection.

1. Set up a connection:

   ```sh
   python3 \
     -m aimtools \
     kubernetes \
     taloslinux \
     . \
     connect
   ```

1. Now continue the deployment.

1. Test `kubectl`.
   This tool connects to the [control plane](https://www.talos.dev/v1.8/learn-more/control-plane/) of the deployed IT-infra.

   ```sh
   dotenv \
     run \
     -- \
     kubectl \
     version

   Client Version: v1.31.2
   Kustomize Version: v5.4.2
   Server Version: v1.31.1
   ```

### To deploy on Podman instance on your macOS or Windows workstation

You need a rootful [Podman machine](https://docs.podman.io/en/stable/markdown/podman-machine.1.html), with [sufficient CPU and especially memory](https://www.talos.dev/v1.8/introduction/system-requirements/) and the default OS image (the implicit default).

Find out how to remove the default Podman machine, if you want.
To create a minimal machine useful for three control plane nodes and two worker nodes, issue:

```sh
podman \
  machine \
  init \
  --cpus 6 \
  --disk-size 150 \
  --now \
  --memory 12288 \
  --timezone utc \
  --verbose \
  --volume "$HOME:$HOME"
```

Use the sample configuration in [`configurations/taloslinux-podman/)`](configurations/taloslinux-podman/).
Adjust e.g., `$AIMTOOLS_OPENSSHCLIENT_PORT` to your actual Podman configuration details, like so:

```sh
podman \
  system \
  connection \
  list

Name URI Identity Default ReadWrite
podman-machine-default ssh://core@127.0.0.1:57542/run/user/501/podman/podman.sock /Users/sanderhan/.local/share/containers/podman/machine/machine false true
podman-machine-default-root ssh://root@127.0.0.1:57542/run/podman/podman.sock /Users/sanderhan/.local/share/containers/podman/machine/machine true true
```

Notice that based on the previous listing, the port appears to be `57542`.

After you've set up Podman and configured the deployment, use the Deployment tool [normally](#to-use).

### To use the Storage Attached Network (SAN) on R29

- Format the `/dev/mapper/lun-aim` block device to e.g., XFS.
- Mount the volume on the hypervisor.
- Add container volume mounts to (subpaths of) this hypervisor volume.

### To use NVIDIA CUDA inside Pods

After you've deployed and set up a tunnel to the deployment instance, you can enable GPU workloads.

On a Ubuntu 24.04-based SRC Workspace:

```sh
# Install NVIDIA drivers.
sudo \
  apt \
  install \
  ubuntu-drivers-common
sudo \
  apt \
  install \
  --reinstall \
  "linux-headers-$(uname -r)"
sudo \
  ubuntu-drivers \
  install \
  nvidia:535-server
# Install the NVIDIA Container Toolkit.
## Follow https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html#installing-with-apt
# Configure the NVIDIA Container Toolkit
sudo \
  nvidia-ctk \
  cdi \
  generate \
  --output=/etc/cdi/nvidia.yaml
# Check if GPU was found.
nvidia-ctk \
  cdi \
  list
```

On your workstation:

```sh
# Set the right KUBECONFIG environment variable at this point (at the latest).
kubectl \
  apply \
  --filename=k8s/runtimeclass-nvidia.yml \
  --validate=strict
# Install NVIDIA device plugin into Kubernetes cluster.
helm \
  repo \
  update
helm \
  install \
  nvidia-device-plugin \
  nvdp/nvidia-device-plugin \
  --create-namespace \
  --namespace nvidia-device-plugin \
  --set deviceListStrategy=cdi-cri \
  --set psp.enabled=true \
  --set runtimeClassName=nvidia
# Loosen Pod Security for NVIDIA pods, since they depend on it.
kubectl \
  label \
  namespace \
  nvidia-device-plugin \
  pod-security.kubernetes.io/enforce=privileged
# Test whether GPU-enabled containers can be created.
kubectl \
  run \
  nvidia-test \
  --image nvdia/cuda:12.5.0-runtime-ubuntu22.04 \
  --namespace=nvidia-device-plugin \
  --overrides '{"spec": {"runtimeClassName": "nvidia"}}' \
  --privileged \
  --restart=Never \
  --rm \
  --stdin=false \
  --tty=false \
  nvidia-smi
# Test with the `k8s/pod-cuda.yml` sample in this repository.
kubectl \
  apply \
  --filename=k8s/pod-cuda.yml \
  --validate=strict
```

### To (re)connect to the control plane (`connect`)

After deploying using the previous instructions, you have a connection to the control plane using `kubectl` and `talosctl`.
Should you have disconnected (e.g., your laptop went to sleep, you logged out, terminated some processes, etc.), then you will need to reconnect manually.

From your deployment configuration directory, issue:

```sh
python3 \
  -m aimtools \
  kubernetes \
  taloslinux \
  . \
  connect
```

From then on, you can use `dotenv run -- talosctl` and `dotenv run -- kubectl`.

### To clean up before a new deployment (`destroy`)

From your deployment configuration directory, issue:

```sh
python3 \
  -m aimtools \
  kubernetes \
  taloslinux \
  . \
  destroy
```
