from __future__ import annotations

from asyncio import TaskGroup
from base64 import standard_b64encode
from io import BytesIO
from ipaddress import IPv6Address, IPv6Interface, IPv6Network
from json import loads as json_loads
from os import environ
from urllib.parse import urlparse

from anyio import Path
from loguru import logger
from ruamel.yaml import YAML

from aimtools.cli import ExecuteError, execute
from aimtools.kubernetes.configure_ssh import configure_ssh
from aimtools.kubernetes.taloslinux import (
    ConfigInfra,
    ConfigKubernetes,
    ConfigTaloslinux,
    IpamNodesNonsegmented,
    Taloslinux,
)


class TunnelError(RuntimeError):
    pass


class TunnelCreateError(TunnelError):
    def __init__(self, service: str) -> None:
        super().__init__("Failed to tunnel to service %s.", service)


class TunnelCarrierCreateError(TunnelError):
    def __init__(self) -> None:
        super().__init__("Failed to create tunnel carrier.")


class NetworknamespaceError(RuntimeError):
    def __init__(self) -> None:
        super().__init__("Network namespace not found.")


class Containers(Taloslinux):
    def __repr__(self) -> str:
        return (
            f"{self.__class__.__name__}(\n"
            f"\t{self.configinfra.name_networkinterface_parent=},\n"
            f"\t{super().__repr__()})\n"
        )

    def __init__(
        self,
        *,
        configinfra: ConfigInfra,
        configkubernetes: ConfigKubernetes,
        configtaloslinux: ConfigTaloslinux,
        path_dir_configuration: Path,
    ) -> None:
        self.path_dir_instance = Path("/run/") / f"taloslinux-{configkubernetes.name_cluster}"
        # TODO: Use container DNS?
        super().__init__(
            configinfra=configinfra,
            configkubernetes=configkubernetes,
            configtaloslinux=configtaloslinux,
            path_dir_configuration=path_dir_configuration,
        )

    async def destroy(self) -> None:
        await execute(
            "ssh",
            "-p",
            str(self.configinfra.opensshclient_port),
            self.configinfra.opensshclient_host,
            "--",
            "podman",
            "container",
            "rm",
            f"--filter=label=app=taloslinux-{self.configkubernetes.name_cluster}",
            "--force",
            "--volumes",
            "--all",
            stderr=logger,
            stdout=logger,
            text=True,
        )
        await execute(
            "ssh",
            "-p",
            str(self.configinfra.opensshclient_port),
            self.configinfra.opensshclient_host,
            "--",
            "podman",
            "network",
            "rm",
            "--force",
            f"taloslinux-{self.configkubernetes.name_cluster}",
            stderr=logger,
            stdout=logger,
            text=True,
        )
        for ipnetwork_nodes in (
            *self.configinfra.ipnetworks_nodes_controlplane,
            *self.configinfra.ipnetworks_nodes_worker,
        ):
            bytesio_stdout = BytesIO()
            command_iptables = f"ip{"6" if isinstance(ipnetwork_nodes, IPv6Network) else ""}tables"
            await execute(
                "ssh",
                "-p",
                str(self.configinfra.opensshclient_port),
                self.configinfra.opensshclient_host,
                "--",
                command_iptables,
                "--list-rules",
                "POSTROUTING",
                "--table",
                "nat",
                stdout=bytesio_stdout,
                stderr=logger,
                text=True,
            )
            iptables_rule_marker = f"--comment taloslinux-{self.configkubernetes.name_cluster}"
            rules = [
                line.replace("-A POSTROUTING ", "")
                for line in bytesio_stdout.getvalue().decode().splitlines()
                if iptables_rule_marker in line
            ]
            for rule in rules:
                await execute(
                    "ssh",
                    "-p",
                    str(self.configinfra.opensshclient_port),
                    self.configinfra.opensshclient_host,
                    "--",
                    command_iptables,
                    "-t",
                    "nat",
                    "--delete",
                    "POSTROUTING",
                    rule,
                    stderr=logger,
                    stdout=logger,
                    text=True,
                )
        for path_socket in (self.path_dir_instance / "kube-apiserver.sock", self.path_dir_instance / "apid.sock"):
            if await path_socket.exists():
                bytesio_stdout = BytesIO()
                _ = await execute(
                    "ssh",
                    "-p",
                    str(self.configinfra.opensshclient_port),
                    self.configinfra.opensshclient_host,
                    "--",
                    "lsof",
                    "-t",
                    "--",
                    str(path_socket),
                    stderr=logger,
                    stdout=bytesio_stdout,
                    text=True,
                )
                if bytesio_stdout.getvalue():
                    await execute(
                        "ssh",
                        "-p",
                        str(self.configinfra.opensshclient_port),
                        self.configinfra.opensshclient_host,
                        "--",
                        "kill",
                        "-HUP",
                        "--",
                        *bytesio_stdout.getvalue().splitlines(),
                        stderr=logger,
                        stdout=logger,
                        text=True,
                    )

    async def connect(self) -> None:
        nsfs_name = await self._get_nsfs_name()
        await self.tunnelize(nsfs_name=nsfs_name)

    async def deploy(self) -> None:
        assert isinstance(self.configinfra.ipam_nodes, IpamNodesNonsegmented)
        await configure_ssh(
            application="taloslinux-tunnel",
            endpoint=self.configinfra.ipaddress_endpoint_k8sapiserver_workstation,
            name_user="root",
            opensshclient_host=self.configinfra.opensshclient_host,
            opensshclient_hostname=self.configinfra.hostname_endpoint_hypervisor,
            path_dir_config_included=self.path_dir_configuration,
        )
        await execute(
            "ssh",
            "-p",
            str(self.configinfra.opensshclient_port),
            self.configinfra.opensshclient_host,
            "--",
            "install",
            "-d",
            "-D",
            "-m",
            "0700",
            "--",
            str(self.path_dir_instance),
            stderr=logger,
            stdout=logger,
            text=True,
        )
        # TODO: Network options: `isolate` and `vlanq`?
        await execute(
            "ssh",
            "-p",
            str(self.configinfra.opensshclient_port),
            self.configinfra.opensshclient_host,
            "--",
            "podman",
            "network",
            "create",
            "--driver=ipvlan",
            "--ipam-driver=host-local",
            f"--label=app=taloslinux-{self.configkubernetes.name_cluster}",
            "--opt=mode=l3s",
            f"--opt=parent={self.configinfra.name_networkinterface_parent}",
            # Podman assigns a /64 IPv6 subnet by default. Since a /64 shouldn't be split up, use a /63 to be
            # split up into worker and control plane subnets.
            # TODO: Reuse later.
            *[
                parameter
                for ipnetwork_nodes in self.configinfra.ipam_nodes.ipnetworks_nodes
                for parameter in (
                    "--subnet",
                    str(ipnetwork_nodes),
                    f"--gateway={next(ipnetwork_nodes.hosts())!s}",
                )
            ],
            *[
                "--ipv6",
                "--",
                f"taloslinux-{self.configkubernetes.name_cluster}",
            ],
            stderr=logger,
            stdout=logger,
            text=True,
        )
        bytesio_stdout = BytesIO()
        _ = await execute(
            "ssh",
            "-p",
            str(self.configinfra.opensshclient_port),
            self.configinfra.opensshclient_host,
            "--",
            "podman",
            "network",
            "inspect",
            "--",
            f"taloslinux-{self.configkubernetes.name_cluster}",
            stderr=logger,
            stdout=bytesio_stdout,
            text=True,
        )
        json_network = json_loads(bytesio_stdout.getvalue())
        for ipnetwork_nodes in self.configinfra.ipam_nodes.ipnetworks_nodes:
            command_iptables = f"ip{"6" if isinstance(ipnetwork_nodes, IPv6Network) else ""}tables"
            _ = await execute(
                "ssh",
                "-p",
                str(self.configinfra.opensshclient_port),
                self.configinfra.opensshclient_host,
                "--",
                command_iptables,
                "--append",
                "POSTROUTING",
                "-m",
                "comment",
                "--comment",
                self.configinfra.opensshclient_host,
                "--jump",
                "SNAT",
                "--out-interface",
                self.configinfra.name_networkinterface_parent,
                "--table",
                "nat",
                "--source",
                str(ipnetwork_nodes),
                "--to-source",
                str(
                    (
                        self.configinfra.ipinterface_hypervisor_ipv6.ip
                        if isinstance(ipnetwork_nodes, IPv6Network)
                        else self.configinfra.ipinterface_hypervisor_ipv4.ip
                    ),
                ),
                stderr=logger,
                stdout=logger,
                text=True,
            )
        for index_node, networkinterfaces_node in enumerate(self.configinfra.networkinterfaces_nodes):
            is_controlplane, name_node = self.configinfra.describe_node(index=index_node)
            await execute(
                "ssh",
                "-p",
                str(self.configinfra.opensshclient_port),
                self.configinfra.opensshclient_host,
                "--",
                "podman",
                "container",
                "create",
                # TODO: Let Talos Linux handle DNS. "--dns=none",
                "--env=PLATFORM=container",
                "--env=USERDATA="
                + standard_b64encode(
                    bytes(
                        await super()
                        .produce_path_configuration_node(index_node=index_node, is_controlplane=is_controlplane)
                        .read_text(encoding="utf-8"),
                        encoding="utf-8",
                    ),
                ).decode(encoding="utf-8"),
                *(["--device", "nvidia.com/gpu=all"] if self.configtaloslinux.cuda else []),
                f"--network={json_network[0]['id']}:"
                + ",".join(
                    [
                        f"ip{("6" if isinstance(networkinterface_node.ipinterface, IPv6Interface)
                                  else "")}={networkinterface_node.ipinterface.ip!s}"
                        for networkinterface_node in networkinterfaces_node
                    ],
                ),
                "--name",
                name_node,
                "--hostname",
                name_node,
                f"--label=app=taloslinux-{self.configkubernetes.name_cluster}",
                f"--pidfile={self.path_dir_instance / name_node!s}.pid",
                "--mount=type=tmpfs,destination=/run",
                "--mount=type=tmpfs,destination=/system",
                "--mount=type=tmpfs,destination=/tmp",
                "--mount=type=volume,destination=/etc/cni",
                "--mount=type=volume,destination=/etc/kubernetes",
                "--mount=type=volume,destination=/opt",
                "--mount=type=volume,destination=/system/state",
                "--mount=type=volume,destination=/usr/etc/udev",
                "--mount=type=volume,destination=/usr/libexec/kubernetes",
                "--mount=type=volume,destination=/var",
                "--privileged",
                "--read-only",
                "--security-opt",
                "seccomp=unconfined",
                "--",
                f"ghcr.io/siderolabs/talos:v{self.configtaloslinux.version}",
                stderr=logger,
                stdout=logger,
                text=True,
            )
        for index_node in range(self.configinfra.n_nodes_controlplane + self.configinfra.n_nodes_worker):
            is_controlplane, name_node = self.configinfra.describe_node(index=index_node)
            await execute(
                "ssh",
                "-p",
                str(self.configinfra.opensshclient_port),
                self.configinfra.opensshclient_host,
                "--",
                "podman",
                "container",
                "start",
                "--",
                # TODO: Factor container name out.
                name_node,
                stderr=logger,
                stdout=logger,
                text=True,
            )
        input("Press any key to continue when connection is up ...")
        await execute(
            "talosctl",
            "--nodes",
            str(self.configinfra.ipaddress_endpoint_k8sapiserver_workstation),
            "bootstrap",
            env={
                **environ.copy(),
                "TALOSCONFIG": str(self.path_file_taloslinux_config_talosconfig),
            },
            stderr=logger,
            stdout=logger,
            text=True,
        )
        await execute(
            "talosctl",
            "--nodes",
            str(self.configinfra.ipaddress_endpoint_k8sapiserver_workstation),
            "health",
            "--control-plane-nodes",
            ",".join(
                str(networkinterfaces_node[0].ipinterface.ip)
                for networkinterfaces_node in self.configinfra.networkinterfaces_nodes_controlplane
            ),
            "--worker-nodes",
            ",".join(
                str(networkinterfaces_node[0].ipinterface.ip)
                for networkinterfaces_node in self.configinfra.networkinterfaces_nodes_worker
            ),
            env={
                **environ.copy(),
                "TALOSCONFIG": str(self.path_file_taloslinux_config_talosconfig),
            },
            stderr=logger,
            stdout=logger,
            text=True,
        )
        await execute(
            "talosctl",
            "--nodes",
            str(self.configinfra.ipaddress_endpoint_k8sapiserver_workstation),
            "kubeconfig",
            str(self.path_file_kubeconfig),
            env={**environ.copy(), "TALOSCONFIG": str(self.path_file_taloslinux_config_talosconfig)},
            text=True,
            stderr=logger,
            stdout=logger,
        )
        documents = []
        async with await self.path_file_kubeconfig.open(mode="r") as file_input:
            with YAML(pure=True) as yaml:
                for document in yaml.load_all(file_input.wrapped):
                    for cluster in document["clusters"]:
                        if cluster["name"] == self.configkubernetes.name_cluster:
                            url_endpoint_k8sapiserver_workstation = urlparse(cluster["cluster"]["server"])
                            url_endpoint_k8sapiserver_workstation = url_endpoint_k8sapiserver_workstation._replace(
                                netloc=f"{f"[{self.configinfra.ipaddress_endpoint_k8sapiserver_workstation!s}]"
                                        if isinstance(self.configinfra.ipaddress_endpoint_k8sapiserver_workstation,
                                                        IPv6Address)
                                        else str(self.configinfra.ipaddress_endpoint_k8sapiserver_workstation)}:6443",
                            ).geturl()
                            cluster["cluster"]["server"] = str(url_endpoint_k8sapiserver_workstation)
                    documents.append(document)
        async with await self.path_file_kubeconfig.open(mode="w") as file_output:
            with YAML(output=file_output.wrapped, pure=True) as yaml:
                yaml.default_flow_style = False
                yaml.explicit_start = True
                yaml.preserve_quotes = True
                for document in documents:
                    yaml.dump(document)

    async def _get_nsfs_name(self) -> str:
        bytesio_stdout = BytesIO()
        _ = await execute(
            "ssh",
            "-p",
            str(self.configinfra.opensshclient_port),
            self.configinfra.opensshclient_host,
            "--",
            "lsns",
            "--type=net",
            "--json",
            stdout=bytesio_stdout,
            stderr=logger,
            text=True,
        )
        namespaces = json_loads(bytesio_stdout.getvalue())
        bytesio_stdout = BytesIO()
        # TODO: Don't hardcode.
        _ = await execute(
            "ssh",
            "-p",
            str(self.configinfra.opensshclient_port),
            self.configinfra.opensshclient_host,
            "--",
            "cat",
            str(self.path_dir_instance / "controlplane-0.pid"),
            stderr=logger,
            stdout=bytesio_stdout,
            text=True,
        )
        pid_controlplane_0 = bytesio_stdout.getvalue()
        for namespace_net in namespaces["namespaces"]:
            if int(namespace_net["pid"]) == int(pid_controlplane_0):
                return Path(namespace_net["nsfs"]).name
        raise NetworknamespaceError()

    async def tunnel_carrier(self) -> None:
        try:
            await execute(
                "ssh",
                "-f",
                "-p",
                str(self.configinfra.opensshclient_port),
                "--",
                f"{self.configinfra.opensshclient_host}-proxy",
                stderr=logger,
                stdout=logger,
                text=True,
            )
        except ExecuteError as exception:
            raise TunnelCarrierCreateError() from exception

    async def tunnel_services(self, service: str, nsfs_name: str) -> None:
        """`service`: `"kube-apiserver"` or `"apid"`."""
        # TODO: Use enum, parameterize.
        endpoint = "127.0.0.1:50000" if service == "apid" else "127.0.0.1:6443"
        # TODO: (infosec) Create Unix socket inside directory with correct permissions to deal with BSD semantics.
        try:
            await execute(
                "ssh",
                "-f",
                "-p",
                str(self.configinfra.opensshclient_port),
                "--",
                self.configinfra.opensshclient_host,
                "setsid",
                "--",
                "socat",
                "-D",
                "-d3",
                f"-lf{self.path_dir_instance / service!s}.log",
                "--experimental",
                f"UNIX-LISTEN:{self.path_dir_instance / service!s}.sock,fork,umask=0177,unlink-early",
                f"TCP:{endpoint},nodelay,netns={nsfs_name}",
                stderr=logger,
                stdout=logger,
                text=True,
            )
        except ExecuteError as exception:
            raise TunnelCreateError(service=service) from exception

    async def tunnelize(self, nsfs_name: str) -> None:
        logger.info("Setting up tunnels ...")
        async with TaskGroup() as taskgroup:
            taskgroup.create_task(
                coro=self.tunnel_carrier(),
                name="tunnel-carrier",
            )
            taskgroup.create_task(
                coro=self.tunnel_services(service="apid", nsfs_name=nsfs_name),
                name="tunnel-apid",
            )
            taskgroup.create_task(
                coro=self.tunnel_services(service="kube-apiserver", nsfs_name=nsfs_name),
                name="tunnel=kube-apiserver",
            )
        logger.info("Tunnel set up.")
