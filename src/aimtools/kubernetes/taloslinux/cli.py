from __future__ import annotations

from ipaddress import IPv4Address, IPv4Interface, IPv6Address, IPv6Interface
from os import environ
from tomllib import loads as tomllib_loads
from typing import TYPE_CHECKING, Any

from aimtools.kubernetes.taloslinux import (
    ConfigInfra,
    ConfigKubernetes,
    ConfigTaloslinux,
    IpamNodesNonsegmented,
    Taloslinux,
    configure_ipam_k8s,
    configure_ipam_nodes,
    configure_loadbalancing_nodes,
)
from aimtools.kubernetes.taloslinux.containers import Containers
from aimtools.kubernetes.taloslinux.proxmox import Proxmox

if TYPE_CHECKING:
    from anyio import Path


async def subcommand(
    *,
    subcommand_taloslinux: str,
    generate_taloslinux_configuration: bool,
    path_dir_configuration: Path,
) -> Taloslinux:
    configuration: dict[str, Any] = {}
    path_file_configuration = path_dir_configuration / "configuration.toml"
    async with await path_file_configuration.open("rb") as file_configuration:
        configuration = tomllib_loads((await file_configuration.read()).decode(encoding="UTF-8"))
    kubernetes = ConfigKubernetes(
        ipam_k8s=configure_ipam_k8s(configuration),
        name_cluster=str(configuration["name_cluster"]),
        version=str(configuration["version_kubernetes"]),
    )
    configtaloslinux = ConfigTaloslinux(
        cuda=bool(configuration["cuda"]),
        dnsresolvers=[
            *[IPv4Address(dnsresolver) for dnsresolver in configuration["dnsresolvers_ipv4"]],
            *[IPv6Address(dnsresolver) for dnsresolver in configuration["dnsresolvers_ipv6"]],
        ],
        generate_taloslinux_configuration=generate_taloslinux_configuration,
        loadbalancing_nodes=configure_loadbalancing_nodes(configuration),
        version=str(configuration["version_taloslinux"]),
    )
    configinfra = ConfigInfra(
        hostname_endpoint_hypervisor=str(configuration["hostname_endpoint_hypervisor"]),
        opensshclient_host=environ["AIMTOOLS_OPENSSHCLIENT_HOST"],
        opensshclient_port=int(environ["AIMTOOLS_OPENSSHCLIENT_PORT"]),
        # TODO: Don't set this default.
        ipaddress_endpoint_k8sapiserver_workstation=IPv4Address("127.0.0.1"),
        ipam_nodes=configure_ipam_nodes(configuration),
        ipinterface_hypervisor_ipv4=IPv4Interface(configuration["ipinterface_hypervisor_ipv4"]),
        ipinterface_hypervisor_ipv6=IPv6Interface(configuration["ipinterface_hypervisor_ipv6"]),
        n_nodes_controlplane=int(configuration["n_nodes_controlplane"]),
        n_nodes_worker=int(configuration["n_nodes_worker"]),
        name_networkinterface_parent=str(configuration["name_networkinterface_parent"]),
        vlanid=int(configuration["vlanid"]),
    )
    platform_str = str(configuration["platform"])
    platform: Containers | Proxmox
    match platform_str:
        case "containers":
            assert isinstance(configinfra.ipam_nodes, IpamNodesNonsegmented)
            # pylint: disable-next=redefined-variable-type
            platform = Containers(
                configinfra=configinfra,
                configkubernetes=kubernetes,
                configtaloslinux=configtaloslinux,
                path_dir_configuration=path_dir_configuration,
            )
        case "proxmox":
            # pylint: disable-next=redefined-variable-type
            platform = Proxmox(
                configinfra=configinfra,
                configkubernetes=kubernetes,
                configtaloslinux=configtaloslinux,
                path_dir_configuration=path_dir_configuration,
            )
        case _:
            msg = f"Platform `{platform_str}` unknown."
            raise NotImplementedError(msg)
    await platform.configure()
    # TODO: Use match case.
    if subcommand_taloslinux == "destroy":
        await platform.destroy()
    elif subcommand_taloslinux == "connect":
        await platform.connect()
    elif subcommand_taloslinux == "deploy":
        await platform.deploy()
    return platform
