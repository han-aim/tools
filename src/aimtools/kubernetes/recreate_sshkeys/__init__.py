"""Recreate an OpenSSH keypair and a host key in a Kubernetes Namespace."""

from __future__ import annotations

from typing import TYPE_CHECKING

from cryptography.hazmat.primitives.asymmetric.ed25519 import Ed25519PrivateKey
from cryptography.hazmat.primitives.serialization import Encoding, NoEncryption, PrivateFormat, PublicFormat
from kubernetes.client import V1ObjectMeta, V1Secret

from aimtools.kubernetes import create_namespaced_secret

if TYPE_CHECKING:
    from kubernetes.client import CoreV1Api


def recreate_sshkey(corev1api: CoreV1Api, namespace: str, name: str) -> None:
    key = Ed25519PrivateKey.generate()
    key_public = key.public_key().public_bytes(
        Encoding.OpenSSH,
        PublicFormat.OpenSSH,
    )
    key_private = key.private_bytes(
        Encoding.PEM,
        PrivateFormat.OpenSSH,
        NoEncryption(),
    )
    secret_key = V1Secret(
        immutable=True,
        metadata=V1ObjectMeta(name=name, namespace=namespace),
        type="kubernetes.io/ssh-auth",
        string_data={
            "ssh-privatekey": key_private.decode(encoding="ascii"),
            "ssh-publickey": key_public.decode(encoding="ascii"),
        },
    )
    create_namespaced_secret(corev1api=corev1api, namespace=namespace, name=name, secret_key=secret_key)


def subcommand(corev1api: CoreV1Api, namespace: str) -> None:
    recreate_sshkey(corev1api=corev1api, namespace=namespace, name="hostkey-sshd")
    recreate_sshkey(corev1api=corev1api, namespace=namespace, name="identity-sshd")
