"""Create a namespace."""

from __future__ import annotations

from kubernetes.client import CoreV1Api, V1Namespace, V1ObjectMeta


def subcommand(corev1api: CoreV1Api, namespace: str) -> None:
    body = V1Namespace(metadata=V1ObjectMeta(name=namespace))
    corev1api.create_namespace(body=body)
