"""Recreate basic authentication credentials as a Kubernetes Secret using `diceware`-generated passphrase."""

from __future__ import annotations

from diceware import get_passphrase
from kubernetes.client import CoreV1Api, V1ObjectMeta, V1Secret

from aimtools.kubernetes import create_namespaced_secret


def subcommand(corev1api: CoreV1Api, namespace: str, name: str, username: str) -> None:
    passphrase = get_passphrase()
    secret_key = V1Secret(
        immutable=True,
        metadata=V1ObjectMeta(name=name, namespace=namespace),
        type="kubernetes.io/basic-auth",
        string_data={
            "password": passphrase,
            "username": username,
        },
    )
    create_namespaced_secret(corev1api=corev1api, namespace=namespace, name=name, secret_key=secret_key)
