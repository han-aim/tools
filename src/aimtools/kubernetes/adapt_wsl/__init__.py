"""Adapt `KUBECONFIG` in the `.env` file to the appropriate path under WSL2."""

from __future__ import annotations

from io import BytesIO
from os import chdir

from anyio import Path
from dotenv import find_dotenv, load_dotenv, set_key
from loguru import logger

from aimtools.cli import execute

# The duplicate part is not worth factoring out as this is deprecated.
# pylint: disable-next=duplicate-code
__deprecated__ = "This is no longer in use without Rancher Desktop."


async def subcommand(path_dir_userprofile: Path, path_input: Path) -> None:
    bytesio_stdout = BytesIO()
    _ = await execute(
        "wslpath",
        "-a",
        str(path_input),
        stderr=logger,
        stdout=bytesio_stdout,
        text=True,
    )
    path_dir_working = Path(bytesio_stdout.getvalue().decode().rstrip("\n"))
    chdir(path_dir_working)
    if not (str_path_file_dotenv := find_dotenv(usecwd=True)):
        message = f"`Could not find `.env` in current working directory {Path.cwd()!s} or up."
        raise RuntimeError(message)
    path_file_dotenv = Path(str_path_file_dotenv)
    load_dotenv(dotenv_path=path_file_dotenv, verbose=True)
    str_path_userprofile_maybe = str(path_dir_userprofile)
    bytesio_stdout = BytesIO()
    _ = await execute(
        "wslpath",
        "-a",
        str_path_userprofile_maybe,
        stderr=logger,
        stdout=bytesio_stdout,
        text=True,
    )
    path_dir_userprofile = Path(bytesio_stdout.getvalue().decode().rstrip("\n"))
    if path_dir_userprofile.is_absolute() and path_dir_userprofile.is_relative_to("/mnt"):
        path_userprofile_kubeconfig = path_dir_userprofile / ".kube" / "config"
        if await path_userprofile_kubeconfig.is_file():
            set_key(
                dotenv_path=path_file_dotenv,
                key_to_set="KUBECONFIG",
                value_to_set=str(path_userprofile_kubeconfig),
            )
        else:
            message = f"`path_userprofile_kubeconfig` ({path_userprofile_kubeconfig!s}) is not a file. "
            raise RuntimeError(message)
    else:
        message = f"`wslpath %USERPROFILE%` returned an unexpected value {path_dir_userprofile!s}. "
        raise RuntimeError(message)
