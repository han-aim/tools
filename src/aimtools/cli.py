from __future__ import annotations

from asyncio import create_subprocess_exec
from asyncio.subprocess import PIPE, Process
from io import BufferedIOBase, BufferedReader, BufferedWriter, BytesIO
from os import PathLike, fspath
from typing import TYPE_CHECKING

from anyio import AsyncFile
from loguru import logger

if TYPE_CHECKING:
    from typing import Any

    from loguru import Logger

    type StrOrBytesPath = str | bytes | PathLike[str] | PathLike[bytes]


def concatenate_command(*command: StrOrBytesPath) -> str:
    commandline: str = ""
    commandline_bytes: bytes = b""
    for segment in command:
        match segment:
            case str():
                commandline += f"{segment} "
            case PathLike():
                segment_fspath = fspath(segment)
                if isinstance(segment_fspath, str):
                    commandline += f" {segment_fspath}"
                else:
                    assert isinstance(segment_fspath, bytes)
                    commandline_bytes += b" " + segment_fspath
            case bytes():
                commandline_bytes += b" " + segment
    return commandline[:-1] if commandline else commandline_bytes[:-1].decode()


class ExecuteError(RuntimeError):
    def __init__(self, *command: StrOrBytesPath, exitstatus: int) -> None:
        commandline = concatenate_command(*command)
        self.command = command
        self.exitstatus = exitstatus
        super().__init__(
            f"The exit status of `{commandline}` is {exitstatus}.",
        )


# The complexity is essential. Repetition could be reduced but that would harm performance efficiency.
async def execute(
    *command: StrOrBytesPath,
    stdin: None | BufferedReader | BytesIO | AsyncFile[bytes] | bytes | bytearray | memoryview = None,
    stderr: None | Logger | int | BufferedWriter | BytesIO | AsyncFile[bytes] = None,
    stdout: None | Logger | int | BufferedWriter | BytesIO | AsyncFile[bytes] = None,
    text: bool = False,
    **kwargs: Any,
) -> Process:
    # TODO: Specify text encoding rather than `text` bool.
    logger.debug(
        "{}",
        concatenate_command(*command),
    )
    process = await create_subprocess_exec(
        *command,
        stderr=stderr if stderr is None else PIPE,
        stdin=stdin if stdin is None else PIPE,
        stdout=stdout if stdout is None else PIPE,
        text=None if text else False,
        **kwargs,
    )
    (data_stdout, data_stderr) = await process.communicate(
        (
            stdin
            if isinstance(stdin, None | bytes | bytearray | memoryview)
            else await stdin.read() if isinstance(stdin, AsyncFile) else stdin.read()
        ),
    )
    if data_stdout and stdout is not None:
        bytesio_data_stdout = BytesIO(data_stdout)
        if isinstance(stdout, BufferedIOBase):
            stdout.writelines(bytesio_data_stdout.readlines())
            stdout.flush()
        else:
            # TODO: Test text encoding on Windows.
            for line in bytesio_data_stdout.readlines():
                entry = line.decode().rstrip() if text else data_stdout
                logger.debug("[stdout] {}", entry)
    if data_stderr and stderr is not None:
        bytesio_data_stderr = BytesIO(data_stderr)
        if isinstance(stderr, BufferedIOBase):
            stderr.writelines(bytesio_data_stderr.readlines())
            stderr.flush()
        else:
            for line in bytesio_data_stderr.readlines():
                entry = line.decode().rstrip() if text else data_stderr
                logger.debug("[stderr] {}", entry)
    if process.returncode:
        raise ExecuteError(
            *command,
            exitstatus=process.returncode,
        )
    return process
