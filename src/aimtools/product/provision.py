"""Produces a Git-based work product based on the standard template."""

from __future__ import annotations

from os import chdir
from shutil import copy2, copytree

from anyio import Path
from loguru import logger

from aimtools.cli import execute

# If value is `None`, the destination matches the source (ignoring the template root directory).
PATH_DESTINATION_TO_SOURCE_COPY_K8S = {
    Path(".gitattributes"): Path(".gitattributes"),
    Path(".gitignore"): Path(".gitignore"),
    Path("k8s/Containerfile.productname"): Path("k8s/alpine/Containerfile"),
    Path(".gitlab-ci.yml"): Path(".gitlab-ci.yml"),
    Path("CODEOWNERS"): Path("CODEOWNERS"),
    Path("README.md"): Path("README.md"),
}
PATH_DESTINATION_TO_SOURCE_COPY_PYTHON = {
    Path("k8s/Containerfile.productname"): Path("python/k8s/alpine/Containerfile"),
    Path(".gitlab-ci.yml"): Path("python/.gitlab-ci.yml"),
    Path("pyproject.toml"): Path("python/pyproject.toml"),
    Path("src/"): Path("python/src/"),
}
PATH_DESTINATION_TO_SOURCE_SYMLINK_K8S = {
    Path(".containerignore"): Path(".containerignore"),
    Path(".editorconfig"): Path(".editorconfig"),
    Path(".lefthook.yml"): Path(".lefthook.yml"),
    Path(".markdownlint.json"): Path(".markdownlint.json"),
    Path(".mega-linter.yml"): Path(".mega-linter.yml"),
    Path(".prettierignore"): Path(".prettierignore"),
    Path(".prettierrc.json"): Path(".prettierrc.json"),
    Path(".v8rrc.yml"): Path(".v8rrc.yml"),
    Path("styles/"): Path("styles/"),
    Path("renovate.json"): Path("renovate.json"),
    Path("lychee.toml"): Path("lychee.toml"),
}
PATH_DESTINATION_TO_SOURCE_SYMLINK_PYTHON = {
    Path(".sourcery.yaml"): Path("python/.sourcery.yaml"),
    Path("pdm.toml"): Path("python/pdm.toml"),
}
NAME_DIR_TEMPLATE = "_template"


async def subcommand(*, path_directory_product: Path, python: bool, kubernetes: bool) -> None:
    input(
        "Press any key to continue filesystem changes for "
        f"{'Kubernetes' if kubernetes else ''}, {'Python' if python else ''} "
        f"under {path_directory_product} ...",
    )
    await path_directory_product.mkdir()
    chdir(path_directory_product)
    path_dir_template = (path_directory_product / NAME_DIR_TEMPLATE).relative_to(
        path_directory_product,
    )
    await execute(
        "git",
        "init",
        "--initial-branch=main",
        stderr=logger,
        stdout=logger,
        text=True,
    )
    await execute(
        "git",
        "submodule",
        "add",
        "--branch",
        "main",
        "--",
        "https://gitlab.com/han-aim/template/main.git",
        path_dir_template.name,
        stderr=logger,
        stdout=logger,
        text=True,
    )
    await execute(
        "git",
        "submodule",
        "update",
        "--init",
        "--recursive",
        stderr=logger,
        stdout=logger,
        text=True,
    )
    await execute(
        "git",
        "checkout",
        "main",
        stderr=logger,
        stdout=logger,
        text=True,
        cwd=path_dir_template,
    )
    paths_copy: dict[Path, Path] = {}
    if kubernetes:
        paths_copy |= PATH_DESTINATION_TO_SOURCE_COPY_K8S
    if python:
        paths_copy |= PATH_DESTINATION_TO_SOURCE_COPY_PYTHON
    for path_target, path_source in paths_copy.items():
        path_original = path_dir_template / path_source
        path_new = path_directory_product / (path_target or path_source)
        if not await path_new.exists():
            path_new_relative = path_new.relative_to(path_directory_product)
            if await path_original.is_dir():
                logger.debug(
                    "Copying directory subtree '{!s}' to '{!s}' ...",
                    path_original,
                    path_new_relative,
                )
                copytree(
                    src=path_original,
                    dst=path_new,
                )
            else:
                if len(path_new_relative.parents) > 1:
                    logger.debug("Making directory up to '{!s}' ...", path_new_relative.parent)
                    await path_new_relative.parent.mkdir(parents=True)
                logger.debug("Copying '{!s}' to '{!s}' ...", path_original, path_new_relative)
                copy2(
                    src=path_original,
                    dst=path_new,
                )
    paths_symlink: dict[Path, Path] = {}
    if kubernetes:
        paths_symlink |= PATH_DESTINATION_TO_SOURCE_SYMLINK_K8S
    if python:
        paths_symlink |= PATH_DESTINATION_TO_SOURCE_SYMLINK_PYTHON
    for path_target, path_source in paths_symlink.items():
        path_symlink_to = path_directory_product / path_dir_template / path_source
        path_to = path_directory_product / (path_target or path_source)
        path_symlink_to_relativetoroot = path_symlink_to.relative_to(path_directory_product)
        path_symlink_from = (
            Path().joinpath(*[".." for _ in path_symlink_to_relativetoroot.parent.parts]) / path_to
            if len(path_symlink_to_relativetoroot.parts) > 1
            else path_to
        )
        logger.debug(
            "Symlinking from '{!s}' to '{!s}' ...",
            path_symlink_from.relative_to(path_directory_product),
            path_symlink_to_relativetoroot,
        )
        await path_symlink_from.symlink_to(
            path_symlink_to_relativetoroot,
            target_is_directory=await path_symlink_to_relativetoroot.is_dir(),
        )
