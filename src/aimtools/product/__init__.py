"""Tools to manage work products."""

from __future__ import annotations

from datetime import UTC, datetime
from io import BytesIO
from os import environ

from loguru import logger
from packaging.version import parse

from aimtools.cli import execute


async def produce_revision() -> str:
    """Produce a revision, an internal representation of a version, of the product that meets PyPA standards.
    See https://packaging.python.org/en/latest/discussions/versioning/."""
    bytesio_stdout = BytesIO()
    # TODO: predict
    if not (commit := environ.get("CI_COMMIT_SHORT_SHA")):
        await execute("git", "rev-parse", "--short", "HEAD", text=True, stdout=bytesio_stdout, stderr=logger)
        commit = bytesio_stdout.getvalue().decode(encoding="ascii").strip()
    version = f"{datetime.now(tz=UTC).isoformat(timespec="milliseconds")
              .replace("-", ".")
              .replace(":", ".")
              .replace("T", ".")
              .replace("Z", "")
              .replace("+", ".")}+git.{commit}"
    # Check whether version is valid according to PyPA.
    _ = parse(version)
    return version
