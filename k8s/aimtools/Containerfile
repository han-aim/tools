# syntax=docker.io/docker/dockerfile:1@sha256:ac85f380a63b13dfcefa89046420e1781752bab202122f8f50032edf31be0021
ARG DISTRO=alpine
ARG REGISTRY_IMAGE_BASE
FROM ${REGISTRY_IMAGE_BASE}k8s-${DISTRO:?}-python:development AS production
ARG DISTRO=alpine
ARG REGISTRY_IMAGE_BASE
ARG REVISION
ARG SELINUXRELABEL=
ARG SOURCE_DATE_EPOCH
ARG TARGETARCH
ARG PDM_BUILD_SCM_VERSION=${REVISION:?}
ARG _INPUTPATH_1=_template/python/k8s/${DISTRO:?}/pdm_install_production.sh
ARG _INPUTPATH_2=_template/python/pdm.toml
ARG _INPUTPATH_3=pdm.lock
ARG _INPUTPATH_4=pyproject.toml
ARG _INPUTPATH_5=README.md
ARG _INPUTPATH_6=src/
COPY ${_INPUTPATH_4:?} /opt/app/${_INPUTPATH_4:?}
RUN \
  --mount=type=bind,source=${_INPUTPATH_1:?},target=/tmp/${_INPUTPATH_1:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_2:?},target=/opt/app/pdm.toml${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_3:?},target=/opt/app/${_INPUTPATH_3:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_5:?},target=/opt/app/${_INPUTPATH_5:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_6:?},target=/opt/app/${_INPUTPATH_5:?}${SELINUXRELABEL} \
  --mount=type=cache,target=/home/unprivileged/.cache/,uid=10001,gid=10001${SELINUXRELABEL} \
  "/tmp/${_INPUTPATH_1:?}"
WORKDIR /opt/app/
LABEL \
  architecture="${TARGETARCH:?}" \
  org.opencontainers.image.authors="Sander.Maijers@han.nl" \
  org.opencontainers.image.base.name="k8s-${DISTRO:?}-python:development" \
  org.opencontainers.image.description="AIM Tools application." \
  org.opencontainers.image.documentation="https://gitlab.com/han-aim/template/base-images/-/blob/main/README.md" \
  org.opencontainers.image.revision="${REVISION:?}" \
  org.opencontainers.image.title="AIM Tools application image" \
  org.opencontainers.image.url="https://gitlab.com/han-aim/template/base-images" \
  org.opencontainers.image.vendor="HAN AIM Research"
FROM ${REGISTRY_IMAGE_BASE}k8s-${DISTRO:?}-python:development AS development
ARG DISTRO=alpine
ARG NAME_PRODUCT
ARG REGISTRY_IMAGE_BASE
ARG REVISION
ARG SELINUXRELABEL=
ARG SOURCE_DATE_EPOCH
ARG TARGETARCH
ARG PDM_BUILD_SCM_VERSION=${REVISION:?}
ARG _INPUTPATH_1=_template/python/k8s/${DISTRO:?}/pdm_install_development.sh
ARG _INPUTPATH_2=_template/python/pdm.toml
ARG _INPUTPATH_3=pdm.lock
ARG _INPUTPATH_4=pyproject.toml
ARG _INPUTPATH_5=README.md
COPY \
  --chown=unprivileged:unprivileged \
  --from=production \
  /opt/app  \
  /opt/app
RUN \
  --mount=type=bind,source=${_INPUTPATH_1:?},target=/tmp/${_INPUTPATH_1:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_2:?},target=/opt/app/pdm.toml${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_3:?},target=/opt/app/${_INPUTPATH_3:?}${SELINUXRELABEL} \
  --mount=type=bind,source=${_INPUTPATH_5:?},target=/opt/app/${_INPUTPATH_5:?}${SELINUXRELABEL} \
  --mount=type=cache,target=/home/unprivileged/.cache/,uid=10001,gid=10001${SELINUXRELABEL} \
  "/tmp/${_INPUTPATH_1:?}"
WORKDIR /opt/app/
LABEL \
  architecture="${TARGETARCH:?}" \
  org.opencontainers.image.authors="Sander.Maijers@han.nl" \
  org.opencontainers.image.base.name="k8s-${DISTRO:?}-python:development" \
  org.opencontainers.image.description="AIM Tools development." \
  org.opencontainers.image.documentation="https://gitlab.com/han-aim/template/base-images/-/blob/main/README.md" \
  org.opencontainers.image.revision="${REVISION:?}" \
  org.opencontainers.image.title="AIM Tools development image" \
  org.opencontainers.image.url="https://gitlab.com/han-aim/template/base-images" \
  org.opencontainers.image.vendor="HAN AIM Research"
