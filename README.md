# AIM Tools<!-- omit in toc -->

- [Introduction](#introduction)
  - [Prerequisites](#prerequisites)
  - [About these instructions](#about-these-instructions)
- [To get started](#to-get-started)
  - [AIM Tools (Python package `aimtools`)](#aim-tools-python-package-aimtools)
  - [To install](#to-install)
    - [From source code](#from-source-code)
    - [Official releases](#official-releases)
  - [To develop](#to-develop)
  - [To use](#to-use)
    - [To lay the groundwork for a new Git-based work product](#to-lay-the-groundwork-for-a-new-git-based-work-product)
    - [To build container images](#to-build-container-images)
    - [To deploy a Datalab IT-platform (Kubernetes based on Talos Linux)](#to-deploy-a-datalab-it-platform-kubernetes-based-on-talos-linux)

The [AIM Tools](https://gitlab.com/han-aim/aimtools/-/blob/main/README.md) help with common, somewhat complicated system administration tasks in a cross-platform, standardized way.
The AIM Tools have a CLI and can also be used as a Python library

## Introduction

This product is meant to assist users and to save time and effort.
In case you have specific requirements, high desire for control and think you must adapt or skip some steps, please see the source code under [`src/`](src/).
However, the author doesn’t support that.
Please consider filing an issue if something should be added or changed in your view.

### Prerequisites

All of the following are requirements:

1. You use a modern computer suitable for software development and management, that is, with at least 8 GiB of RAM and a 4-core CPU.
1. Said computer runs at least Windows, MacOS or Linux in the most up-to-date edition and version.
   Linux is supported fully for the tasks this tutorial covers, but explicit instructions are left out as Linux is diverse and individual users normally know best how to adapt the instructions to their case.
   Older versions or editions may be suitable but are not supported by the author of this tutorial.
   Windows Home edition is not supported.
1. Said computer is connected to the Internet.
1. You have at least a basic proficiency in:
   - Administering said computer.
     - Installing software and nagivating an official website of a technical software product.
     - Finding settings windows within applications.
   - Using command-line programs from a terminal.
   - CPython and the virtual environment concept.
1. You can and are authorized to administer said computer.

### About these instructions

1. When ‘(your/the) computer’ is mentioned in this tutorial, it refers to this and only this computer.
   Perform all steps on the same computer (but you may repeat the sequence on other computers).

1. When you are requested to install something.

   1. Install it without changing the default settings.
   1. Install it at the latest version available to you through the means suggested in the instruction.
   1. ⚠️ Some software, under Windows in particular, should be installed in a specific way.

   Therefore, always run each command, even if you believe you have installed that particular software product already in the past.
   Try to make sure your historical settings for software don’t conflict with the instructions.
   If you have already installed it before and in the specific way this tutorial prescribes, at minimum update to the latest version (as defined in the previous item).

1. Always copy and paste the instructions instead of entering them.
   In the author’s experience, typos can cause confusing faults, and everyone at some point makes a typo.
   Moreover, typing would take a substantial proportion of the time spent to perform this tutorial.

1. Perform each step from top to bottom, left to right.
   Keep track of what you have done.

1. ⚠️ Always issue the commands in this tutorial from within the directory of your working copy of this repository.
   That is, the directory containing this `README` file.

If something does not work or surprises you:

1. Record technical information that may be relevant, including at least:

   - Computer product type/number.
   - Operating system version.
   - Output shown by the programs in the current and previous steps.

1. Contact the author of this tutorial by e-mail.

## To get started

AIM Tools is to be used by developers and other IT professionals, and depending on what features you use in what way, you will need to install a range of dependencies.
Under Linux, you can often install packages from the standard package manager.
Alternatively, under macOS, you may use [Homebrew packages](https://formulae.brew.sh/).
Under Windows, you may use the [Microsoft Store](https://apps.microsoft.com/detail/9ncvdn91xzqp).
The development container image provided in this GitLab Project’s Container Registry has all necessary tools already.

- [Git](https://git-scm.com/) is a version control system.
  Homebrew package: `git`.
- Rsync is a command-line program to transfer/synchronize data efficiently between file systems.
  This tool is the main user interface for the data transfer capability of any data pipeline we develop.
  `rsync`
- [OpenSSH](https://www.openssh.com/manual.html) provides a secure connection between your computer and the data pipeline.
  Homebrew package: `openssh`.
- [Podman](https://podman.io/docs) is a suite of container tools.
  You need it to run containers and deploy the Datalab IT-platform locally.
  Homebrew package: `podman`.
- [PDM](https://pdm-project.org/en/latest/#installation).
  Homebrew package: `pdm`.
- Install [`uv`](https://docs.astral.sh/uv/getting-started/installation/)
- A lot of software and tooling we develop and use depends on Python.
  Install exactly [CPython 3.12](https://www.python.org/downloads/release/python-3120/) and avoid that the other tools listed below pick up alternative Python installations.
  The simplest way to do that is by not having installed any competing versions.
  Use e.g., `uv` to [install and manage Python installations](https://docs.astral.sh/uv/concepts/python-versions/#installing-a-python-version).
- Install [Visual Studio Code](https://code.visualstudio.com/).
  Homebrew package: `visual-studio-code`.
- [`markdownlint-cli2`](https://github.com/DavidAnson/markdownlint-cli2).
- [Prettier](https://prettier.io/docs/en/install.html).
- [ShellCheck](https://github.com/koalaman/shellcheck?tab=readme-ov-file#installing).
- [`shfmt`](https://github.com/mvdan/sh).
- Rust Cargo and [`rustfmt`](https://github.com/rust-lang/rustfmt).
- [Clippy](https://doc.rust-lang.org/clippy/installation.html).
- [Lefthook](https://github.com/evilmartians/lefthook) and `uv` are automatically installed on per-product basis when you issue `pdm install --dev`.

See [the standard product template docs](_template/README.md) to find out more about the purpose and configuration of many of the tools listed.

### AIM Tools (Python package `aimtools`)

### To install

You needn’t install AIM Tools separately in most cases, since it should normally be included with the dev dependencies of all of our products.
Use the following instructions to perform a stand-alone installation.

#### From source code

```sh
pdm install
```

#### Official releases

Should you want to have a stand-alone installation of AIM Tools (and avoid PDM/handling source code), issue the following.
Adapt to your operating system, e.g., the command to activate the virtual environment.

```sh
uv \
  venv
source \
  .venv/bin/activate
uv \
  pip \
  install \
  --extra-index-url https://gitlab.com/api/v4/projects/50625041/packages/pypi/simple \
  --only-binary ':all:' \
  --upgrade \
  aimtools
```

### To develop

To install the package and development tooling, run:

```sh
pdm install --dev
lefthook install
```

### To use

#### To lay the groundwork for a new Git-based work product

Choose a still non-existent directory for your Git-based work product (to be).
The leading directories (i.e., its parents) must exist beforehand.

Then, follow the instructions of the following command:

```sh
python3 -m aimtools product provision --help
```

#### To build container images

- Configure builds in a [`.env`](https://github.com/theskumar/python-dotenv/blob/main/README.md) (`dotenv`) file, based on e.g. [`.env.sample`](.env.sample).

- Activate the virtual environment in which you installed `aimtools`.

- Next, run the following commands.

  ```sh
  dotenv set TARGET development
  python3 -m aimtools container build --tool podman
  ```

  Then do the following:

- Override `TARGET` as appropriate using, e.g., `dotenv set`, for example, when successively building multiple targets.

- Then, invoke the appropriate `--tool`.
  You can use `dockerd`, [`nerdctl`](https://github.com/containerd/nerdctl) or [`podman`](https://podman.io/).
  For building a container image efficiently, rootless and under CI, with (limited) pre-configured Kubernetes support, use `podman`.
  In other words, you could use this as part of a GitLab CI pipeline.

#### To deploy a Datalab IT-platform (Kubernetes based on Talos Linux)

You can use various Kubernetes tools via:

```sh
python3 -m aimtools kubernetes --help
```

Please see [`src/aimtools/kubernetes/taloslinux/README.md`](src/aimtools/kubernetes/taloslinux/README.md) for the Datalab IT-platform specifically.
