from __future__ import annotations

from sys import executable

import pytest
from anyio import Path

from aimtools.cli import ExecuteError, execute


@pytest.mark.asyncio(loop_scope="module")
async def test_execute_failure_log() -> None:
    exitstatus_expected = 2
    with pytest.raises(ExecuteError):
        try:
            _ = await execute(executable, "-Y", text=True)
        except ExecuteError as exception:
            assert exception.exitstatus == exitstatus_expected
            raise


@pytest.mark.asyncio(loop_scope="module")
async def test_execute_success_stdout_file(tmp_path: Path) -> None:
    file_stdout = await Path(tmp_path / "stdout.txt").open(mode="wb")
    async with file_stdout:
        process = await execute(executable, "-V", text=True, stdout=file_stdout)
        assert not process.returncode
