from __future__ import annotations

from io import BytesIO, StringIO
from typing import TYPE_CHECKING

import pytest
from anyio import open_file, wrap_file

from aimtools.container.build import _read_last_line

if TYPE_CHECKING:
    from pathlib import Path


@pytest.mark.asyncio(loop_scope="module")
async def test_read_last_line_mock_bytesio() -> None:
    mockfile = wrap_file(BytesIO())
    await mockfile.write(b"Test1\nTest2")
    await mockfile.flush()
    assert await _read_last_line(file=mockfile) == b"Test2"


@pytest.mark.asyncio(loop_scope="module")
async def test_read_last_line_mock_stringio() -> None:
    mockfile = wrap_file(StringIO())
    await mockfile.write("Test1\nTest2")
    await mockfile.flush()
    with pytest.raises(OSError):
        # Argument type must be incorrect to test exception throwing.
        await _read_last_line(file=mockfile)  # type: ignore[arg-type]


@pytest.mark.asyncio(loop_scope="module")
async def test_read_last_line_text(tmp_path: Path) -> None:
    async with await open_file(file=tmp_path / "file.txt", mode="w+b") as file:
        await file.write(b"Test1\nTest2")
        await file.flush()
        assert await _read_last_line(file=file) == b"Test2"


@pytest.mark.asyncio(loop_scope="module")
async def test_read_last_line_triplenewline_text(tmp_path: Path) -> None:
    async with await open_file(file=tmp_path / "file.txt", mode="w+b") as file:
        await file.write(b"\n\n\nT")
        await file.flush()
        assert await _read_last_line(file=file) == b"T"


@pytest.mark.asyncio(loop_scope="module")
async def test_read_last_line_textnewline(tmp_path: Path) -> None:
    async with await open_file(file=tmp_path / "file.txt", mode="w+b") as file:
        await file.write(b"Test1\nTest2\n")
        await file.flush()
        assert await _read_last_line(file=file) == b"Test2\n"


@pytest.mark.asyncio(loop_scope="module")
async def test_read_last_line_doublenewline(tmp_path: Path) -> None:
    async with await open_file(file=tmp_path / "file.txt", mode="w+b") as file:
        await file.write(b"Test1\nTest2\n\n")
        await file.flush()
        assert await _read_last_line(file=file) == b"\n"


@pytest.mark.asyncio(loop_scope="module")
async def test_read_last_line_triplenewline(tmp_path: Path) -> None:
    async with await open_file(file=tmp_path / "file.txt", mode="w+b") as file:
        await file.write(b"Test1\nTest2\n\n\n")
        await file.flush()
        assert await _read_last_line(file=file) == b"\n"


@pytest.mark.asyncio(loop_scope="module")
async def test_read_last_line_empty(tmp_path: Path) -> None:
    async with await open_file(file=tmp_path / "file.txt", mode="w+b") as file:
        await file.write(b"")
        await file.flush()
        assert await _read_last_line(file=file) == b""


@pytest.mark.asyncio(loop_scope="module")
async def test_read_last_line_space(tmp_path: Path) -> None:
    async with await open_file(file=tmp_path / "file.txt", mode="w+b") as file:
        await file.write(b" ")
        await file.flush()
        assert await _read_last_line(file=file) == b" "


@pytest.mark.asyncio(loop_scope="module")
async def test_read_last_line_justnewline(tmp_path: Path) -> None:
    async with await open_file(file=tmp_path / "file.txt", mode="w+b") as file:
        await file.write(b"\n")
        await file.flush()
        assert await _read_last_line(file=file) == b"\n"


@pytest.mark.asyncio(loop_scope="module")
async def test_read_last_line_justnewline_skipfinalseparator(tmp_path: Path) -> None:
    async with await open_file(file=tmp_path / "file.txt", mode="w+b") as file:
        await file.write(b"\n")
        await file.flush()
        assert await _read_last_line(file=file, skip_final_separator=False) == b""


@pytest.mark.asyncio(loop_scope="module")
async def test_read_last_line_imageid(tmp_path: Path) -> None:
    async with await open_file(file=tmp_path / "file.txt", mode="w+b") as file:
        await file.write(b"sha256:eafe86f57306a5082cdf5d713a891f45c31ff45d778cbaade51a9fc6d456eab0")
        await file.flush()
        assert (
            await _read_last_line(file=file)
            == b"sha256:eafe86f57306a5082cdf5d713a891f45c31ff45d778cbaade51a9fc6d456eab0"
        )


@pytest.mark.asyncio(loop_scope="module")
async def test_read_last_line_nonewline(tmp_path: Path) -> None:
    async with await open_file(file=tmp_path / "file.txt", mode="w+b") as file:
        await file.write(b"Test")
        await file.flush()
        assert await _read_last_line(file=file) == b"Test"
